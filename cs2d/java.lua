local socket = require("socket")

java = {}

-- vars
java.directory = "sys/lua/java"

-- config
java.config = dofile(java.directory.."/config.lua")

-- load
dofile(java.directory.."/utility.lua")
dofile(java.directory.."/extra/pickle.lua")

-- start
java.stable = false
java.checkSocket()

function java.receive()
	java.checkSocket()
	local data, err = java.connection:receive(java.config.sizeLength)
	if (data == nil) then
		java.socketFail(err)
	end

	local data_length = tonumber(data)
	java.checkSocket()
	local data, err = java.connection:receive(data_length)
	if (data == nil) then
		java.socketFail(err)
	end

	-- print("RAW RECV: "..data)

	local package = unpickle(data)

	return package
end

function java.send(package)
	-- print("package: "..tostring(package))
	local data = pickle(package)
	local dataLengthString = java.leftPad(string.len(data), java.config.sizeLength, " ")
	java.checkSocket()
	local range, err = java.connection:send(dataLengthString..data)
	if (err ~= nil) then
		java.socketFail(err)
	end
end

function java.hook_sequence(hook, args)

	-- print("sending "..hook.." args: "..tostring(args).." (length: "..#args..")")

	-- hook start
	java.send({
		type = "HOOK_START",
		hook = hook,
		args = args
	})

	-- receive
	while (true) do
		local package = java.receive()
		-- print("RECV: "..package.type)

		-- LUA CALL --
		if (package.type == "LUA_CALL") then
			local chunk = loadstring("return "..package.code)
			local chunkReturn, err = chunk()
			if (err ~= nil) then
				error("received code failed: "..err)
			end
			-- serialize and response
			local response = {
				type = "LUA_RETURN",
				value = chunkReturn
			}
			-- print("return value: "..tostring(chunkReturn).." from "..package.code)
			java.send(response)
		end

		-- LUA CALL METHOD --
		if (package.type == "LUA_CALL_METHOD") then
			local func = _G[package.method]
			if (func == nil) then
				error("unknown func: "..package.method)
			end
			local success, chunkReturn = pcall(function()
				return func(unpack(package.args))
			end)
			if (success == false) then
				error("failed to execute "..package.method.." due to: "..chunkReturn)
			end
			-- serialize and response
			local response = {
				type = "LUA_RETURN",
				value = chunkReturn
			}
			-- print("return value: "..tostring(chunkReturn).." from "..package.code)
			java.send(response)
		end

		-- HOOK END --
		if (package.type == "HOOK_END") then
			-- print("recv end: "..package.result.." type: "..type(package.result))
			return package.result
		end
	end
end

function java.socketFail(err)
	java.stable = false
	error("Socket failure: "..tostring(err))
end

-- HOOKS
java.hooks = {}

function java.hooks.register(name)
	addhook(name, "java.hooks."..name, java.config.hookPriority)
end

function java.hooks.__index(t, key)
	return function(...)
		local args = {...}
		local result = java.hook_sequence(key, args)
		-- print(key.." -> "..tostring(result))
		return result
	end
end

setmetatable(java.hooks, java.hooks)

-- BASIC
java.hooks.register("break")
java.hooks.register("endround")
java.hooks.register("httpdata")
-- java.hooks.register("log")
java.hooks.register("mapchange")
java.hooks.register("parse")
java.hooks.register("projectile")
java.hooks.register("projectile_impact")
java.hooks.register("rcon")
java.hooks.register("shutdown")
java.hooks.register("startround")

-- TIME
java.hooks.register("always")
java.hooks.register("minute")
java.hooks.register("ms100")
java.hooks.register("second")

-- PLAYER
java.hooks.register("assist")
java.hooks.register("attack")
java.hooks.register("attack2")
java.hooks.register("bombdefuse")
java.hooks.register("bombexplode")
java.hooks.register("bombplant")
java.hooks.register("build")
java.hooks.register("buildattempt")
java.hooks.register("buy")
java.hooks.register("clientdata")
java.hooks.register("clientsetting")
java.hooks.register("collect")
java.hooks.register("connect")
java.hooks.register("connect_attempt")
java.hooks.register("connect_initplayer")
java.hooks.register("die")
java.hooks.register("disconnect")
java.hooks.register("dominate")
java.hooks.register("drop")
java.hooks.register("flagcapture")
java.hooks.register("flagtake")
java.hooks.register("flashlight")
java.hooks.register("hit")
java.hooks.register("hostagedamage")
java.hooks.register("hostagekill")
java.hooks.register("hostagerescue")
java.hooks.register("hostageuse")
java.hooks.register("itemfadeout")
java.hooks.register("join")
java.hooks.register("key")
java.hooks.register("kill")
java.hooks.register("leave")
java.hooks.register("menu")
java.hooks.register("move")
java.hooks.register("movetile")
java.hooks.register("name")
java.hooks.register("radio")
java.hooks.register("reload")
java.hooks.register("say")
java.hooks.register("sayteam")
java.hooks.register("select")
java.hooks.register("serveraction")
java.hooks.register("shieldhit")
java.hooks.register("spawn")
java.hooks.register("specswitch")
java.hooks.register("spray")
java.hooks.register("suicide")
java.hooks.register("team")
java.hooks.register("use")
java.hooks.register("usebutton")
java.hooks.register("vipescape")
java.hooks.register("voice")
java.hooks.register("vote")
java.hooks.register("walkover")

-- OBJECT
java.hooks.register("hitzone")
java.hooks.register("objectdamage")
java.hooks.register("objectkill")
java.hooks.register("objectupgrade")
java.hooks.register("turretscan")

-- init
java.hook_sequence("init", {})

java.print("Ready!")
