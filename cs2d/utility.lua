function java.print(text)
	print("[Java] "..text)
end

function java.leftPad(str, length, filler)
	return java.pad(str, length, function(str)
		return filler..str
	end)
end

function java.rightPad(str, length, filler)
	return java.pad(str, length, function(str)
		return str..filler
	end)
end

function java.pad(str, length, padder)
	while (string.len(str) < length) do
		str = padder(str)
	end
	return str
end

function java.checkSocket()
	if (java.stable == false) then
		print("attempting to connect to "..java.config.address..":"..java.config.port)
		java.connection, err = socket.connect(java.config.address, java.config.port)
		if (err ~= nil) then
			error("failed to connect: "..err)
		end
		java.print("Connected through: "..tostring(java.connection))
		java.stable = true
	end
end
