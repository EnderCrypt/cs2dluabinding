/************************************************************************
 * Cs2dLuaBinding by EnderCrypt                                         *
 * Copyright (C) 2022                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.library.cs2dlua.connector;


import java.io.IOException;
import java.net.ServerSocket;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import endercrypt.library.cs2dlua.cs2d.Cs2d;
import endercrypt.library.cs2dlua.hook.HookListener;
import endercrypt.library.cs2dlua.hook.listener.MasterListener;
import endercrypt.library.cs2dlua.lua.LuaManager;
import endercrypt.library.cs2dlua.message.factory.Cs2dMessageFactory;


public class Cs2dConnector
{
	private static final Logger logger = LogManager.getLogger(Cs2dConnector.class);
	
	public static Cs2dConnector connect(int port) throws IOException
	{
		Cs2dConnector cs2dConnector = new Cs2dConnector(port);
		cs2dConnector.start();
		return cs2dConnector;
	}
	
	private final Cs2dMessageFactory cs2dMessageFactory = new Cs2dMessageFactory();
	private final LuaManager luaManager = new LuaManager();
	
	private final ServerSocket server;
	private final Thread threadAccept = new Thread(new Cs2dConnectorThreadAccept(this), "Cs2dConnector-PostListener");
	private final Thread threadPing = new Thread(new Cs2dConnectorThreadPing(this), "Cs2dConnector-Ping");
	private final Thread threadListen = new Thread(new Cs2dConnectorThreadReceiver(this), "Cs2dConnector-Listen");
	
	private final MasterListener masterListener = new MasterListener();
	private final Cs2dConnectorTransmitter transmitter = new Cs2dConnectorTransmitter(this);
	private final Cs2dConnectionHolder connectionHolder = new Cs2dConnectionHolder();
	
	private final Cs2d cs2d = new Cs2d(this);
	
	private Cs2dConnector(int port) throws IOException
	{
		server = new ServerSocket(port);
	}
	
	protected ServerSocket getServer()
	{
		return server;
	}
	
	protected Cs2dConnectionHolder getConnectionHolder()
	{
		return connectionHolder;
	}
	
	public Cs2d getCs2d()
	{
		return cs2d;
	}
	
	public MasterListener getMasterListener()
	{
		return masterListener;
	}
	
	public Cs2dMessageFactory getCs2dMessageFactory()
	{
		return cs2dMessageFactory;
	}
	
	public LuaManager getLuaManager()
	{
		return luaManager;
	}
	
	public Cs2dConnectorTransmitter getTransmitter()
	{
		return transmitter;
	}
	
	private void start()
	{
		threadAccept.start();
		threadPing.start();
		threadListen.start();
		logger.info("Cs2dConnector server started!");
	}
	
	public void register(HookListener listener)
	{
		masterListener.addListener(listener);
	}
	
	protected void socketFail(String reason, Throwable e)
	{
		connectionHolder.remove();
		logger.error(reason, e);
		logger.info("Connection dropped!");
	}
	
	public void shutdown()
	{
		try
		{
			threadAccept.interrupt();
			threadPing.interrupt();
			threadListen.interrupt();
			server.close();
		}
		catch (IOException e)
		{
			logger.error("failed to gracefully shut down", e);
		}
	}
}
