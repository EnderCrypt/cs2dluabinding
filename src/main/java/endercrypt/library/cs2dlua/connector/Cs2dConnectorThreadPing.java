/************************************************************************
 * Cs2dLuaBinding by EnderCrypt                                         *
 * Copyright (C) 2022                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.library.cs2dlua.connector;


import java.util.Objects;

import endercrypt.library.cs2dlua.message.types.Cs2dPing;
import endercrypt.library.cs2dlua.utility.BasicThread;


public class Cs2dConnectorThreadPing extends BasicThread
{
	private Cs2dConnector connector;
	
	public Cs2dConnectorThreadPing(Cs2dConnector connector)
	{
		this.connector = Objects.requireNonNull(connector, "connector");
	}
	
	@Override
	protected void configureThread(Thread thread)
	{
		thread.setName("Cs2d Connector Ping");
	}
	
	@Override
	protected void runThread() throws Exception
	{
		while (true)
		{
			Thread.sleep(100);
			
			connector.getTransmitter().send(new Cs2dPing());
		}
	}
}
