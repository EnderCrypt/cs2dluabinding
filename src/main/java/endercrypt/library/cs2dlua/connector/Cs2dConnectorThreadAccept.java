/************************************************************************
 * Cs2dLuaBinding by EnderCrypt                                         *
 * Copyright (C) 2022                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.library.cs2dlua.connector;


import java.io.IOException;
import java.net.Socket;
import java.util.Objects;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import endercrypt.library.cs2dlua.utility.BasicThread;


public class Cs2dConnectorThreadAccept extends BasicThread
{
	private static final Logger logger = LogManager.getLogger(Cs2dConnectorThreadAccept.class);
	
	private Cs2dConnector connector;
	
	public Cs2dConnectorThreadAccept(Cs2dConnector connector)
	{
		this.connector = Objects.requireNonNull(connector, "connector");
	}
	
	@Override
	protected void configureThread(Thread thread)
	{
		thread.setName("Cs2d Connector Socket Accepter");
	}
	
	@Override
	protected void runThread() throws Exception
	{
		while (true)
		{
			try
			{
				handleSocketConnection(connector.getServer().accept());
			}
			catch (IOException e)
			{
				logger.error("Failed to receieve socket!", e);
			}
		}
	}
	
	private void handleSocketConnection(Socket socket) throws IOException
	{
		if (isAllowedToAcceptConnection())
		{
			attachConnection(socket);
		}
		else
		{
			dropConnection(socket);
		}
	}
	
	private boolean isAllowedToAcceptConnection()
	{
		return connector.getConnectionHolder().exists() == false || connector.getConnectionHolder().retrieve().isDead();
	}
	
	private void attachConnection(Socket socket) throws IOException
	{
		logger.info("Attached connection from " + socket.getRemoteSocketAddress());
		connector.getConnectionHolder().set(new Cs2dConnection(connector, socket));
	}
	
	private void dropConnection(Socket socket) throws IOException
	{
		logger.warn("Ignored connection " + socket.getRemoteSocketAddress());
		socket.close();
	}
}
