/************************************************************************
 * Cs2dLuaBinding by EnderCrypt                                         *
 * Copyright (C) 2022                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.library.cs2dlua.connector;


import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.Varargs;

import endercrypt.library.cs2dlua.message.factory.Cs2dMessage;
import endercrypt.library.cs2dlua.message.types.Cs2dHookEnd;
import endercrypt.library.cs2dlua.message.types.Cs2dHookStart;
import endercrypt.library.cs2dlua.utility.BasicThread;


public class Cs2dConnectorThreadReceiver extends BasicThread
{
	private static final Logger logger = LogManager.getLogger(Cs2dConnectorThreadReceiver.class);
	
	private Cs2dConnector connector;
	
	public Cs2dConnectorThreadReceiver(Cs2dConnector connector)
	{
		this.connector = Objects.requireNonNull(connector, "connector");
	}
	
	@Override
	protected void configureThread(Thread thread)
	{
		thread.setName("Cs2d Connector Socket Receiver");
	}
	
	@Override
	protected void runThread() throws Exception
	{
		while (true)
		{
			try
			{
				receiveData();
			}
			catch (IOException e)
			{
				connector.socketFail("failed to listen", e);
			}
		}
	}
	
	private void receiveData() throws IOException
	{
		Cs2dMessage message = connector.getConnectionHolder().retrieve().receive();
		if (message instanceof Cs2dHookStart)
		{
			handleHookStart((Cs2dHookStart) message);
		}
	}
	
	private void handleHookStart(Cs2dHookStart hookStart)
	{
		LuaValue result = invokeHookListeners(hookStart);
		log(hookStart.getHook(), result);
		connector.getTransmitter().send(new Cs2dHookEnd(result));
	}
	
	private final List<String> noLogHooks = Arrays.asList("always", "ms100", "second");
	
	private void log(String hook, LuaValue result)
	{
		if (noLogHooks.contains(hook) == false)
		{
			logger.debug("Hook: " + hook + " -> " + result);
		}
	}
	
	private LuaValue invokeHookListeners(Cs2dHookStart hookStart)
	{
		String hook = hookStart.getHook();
		Varargs args = LuaValue.varargsOf(hookStart.getArgs().stream().toArray(LuaValue[]::new));
		return connector.getMasterListener().execute(hook, args);
	}
}
