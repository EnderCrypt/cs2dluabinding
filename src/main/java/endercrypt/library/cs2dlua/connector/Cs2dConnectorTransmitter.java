/************************************************************************
 * Cs2dLuaBinding by EnderCrypt                                         *
 * Copyright (C) 2022                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.library.cs2dlua.connector;


import java.io.IOException;
import java.util.Objects;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.luaj.vm2.LuaError;
import org.luaj.vm2.LuaValue;

import endercrypt.library.cs2dlua.message.EventType;
import endercrypt.library.cs2dlua.message.factory.Cs2dMessage;
import endercrypt.library.cs2dlua.message.types.Cs2dLuaCall;
import endercrypt.library.cs2dlua.message.types.Cs2dLuaCallMethod;
import endercrypt.library.cs2dlua.message.types.Cs2dLuaReturn;


public class Cs2dConnectorTransmitter
{
	private static final Logger logger = LogManager.getLogger(Cs2dConnectorTransmitter.class);
	
	private Cs2dConnector connector;
	
	public Cs2dConnectorTransmitter(Cs2dConnector connector)
	{
		this.connector = Objects.requireNonNull(connector, "connector");
	}
	
	public void parse(String command)
	{
		call("parse", LuaValue.valueOf(command));
	}
	
	public LuaValue call(String method, LuaValue... args)
	{
		return send(Cs2dLuaCallMethod.assemble(method, args));
	}
	
	public LuaValue evaluate(String code)
	{
		return send(new Cs2dLuaCall(code));
	}
	
	public LuaValue send(Cs2dMessage message)
	{
		Objects.requireNonNull(message, "message");
		while (true)
		{
			try
			{
				return sendAttempt(message);
			}
			catch (IOException | LuaError e)
			{
				connector.socketFail("failed to send " + message + " to active connection", e);
			}
		}
	}
	
	private LuaValue sendAttempt(Cs2dMessage message) throws IOException
	{
		if (connector.getConnectionHolder().exists() == false)
		{
			logger.trace("Thread " + Thread.currentThread().getName() + " awaiting connection ...");
		}
		
		connector.getConnectionHolder().retrieve().send(message);
		
		return handleResponseSequence(message);
	}
	
	private LuaValue handleResponseSequence(Cs2dMessage message) throws IOException
	{
		if (message.getType() != EventType.luaCall && message.getType() != EventType.luaCallMethod)
		{
			return null;
		}
		
		logger.trace("Awaiting response ...");
		Cs2dLuaReturn luaReturn = receiveLuaResponse();
		checkReturnValue(luaReturn);
		return luaReturn.getValue();
	}
	
	private Cs2dLuaReturn receiveLuaResponse() throws IOException
	{
		Cs2dMessage response = connector.getConnectionHolder().retrieve().receive();
		if ((response instanceof Cs2dLuaReturn) == false)
		{
			throw new IOException("illegal response message type: " + response.getType());
		}
		return (Cs2dLuaReturn) response;
	}
	
	private void checkReturnValue(Cs2dLuaReturn luaReturn)
	{
		if (luaReturn.getError() != null)
		{
			throw new LuaError(luaReturn.getError());
		}
	}
}
