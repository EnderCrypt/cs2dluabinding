/************************************************************************
 * Cs2dLuaBinding by EnderCrypt                                         *
 * Copyright (C) 2022                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.library.cs2dlua.connector;


import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.Objects;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.luaj.vm2.LuaTable;

import endercrypt.library.cs2dlua.message.factory.Cs2dMessage;


public class Cs2dConnection
{
	private static final Logger logger = LogManager.getLogger(Cs2dConnection.class);
	
	private static final int SIZE_LENGTH = 4;
	
	private final Cs2dConnector cs2dConnetor;
	private final Socket socket;
	private final InputStream input;
	private final OutputStream output;
	
	private boolean alive = true;
	
	protected Cs2dConnection(Cs2dConnector cs2dConnector, Socket socket) throws IOException
	{
		this.cs2dConnetor = cs2dConnector;
		this.socket = socket;
		this.input = socket.getInputStream();
		this.output = socket.getOutputStream();
	}
	
	public boolean isAlive()
	{
		return alive;
	}
	
	public boolean isDead()
	{
		return isAlive() == false;
	}
	
	private void requireAlive() throws IOException
	{
		if (isDead())
		{
			throw new IOException("connection dead");
		}
	}
	
	public void send(Cs2dMessage message) throws IOException
	{
		Objects.requireNonNull(message, "message");
		requireAlive();
		try
		{
			String stringData = message.assemblePackage(cs2dConnetor.getLuaManager());
			byte[] byteData = stringData.getBytes();
			output.write(byteData);
			output.flush();
			logger.trace("sent " + message.getClass().getSimpleName() + " package (" + byteData.length + " bytes)\n" + stringData);
		}
		catch (IOException e)
		{
			logger.error("failed to send data", e);
			shutdown();
			throw e;
		}
	}
	
	private byte[] receiveArray(int size) throws IOException
	{
		byte[] buffer = new byte[size];
		int bufferLength = input.read(buffer);
		if (bufferLength != size)
		{
			throw new IOException("failed to receive data of length " + size);
		}
		return buffer;
	}
	
	private int receiveLength() throws IOException
	{
		requireAlive();
		byte[] data = receiveArray(SIZE_LENGTH);
		String message = new String(data);
		message = message.trim();
		return Integer.parseInt(message);
	}
	
	public Cs2dMessage receive() throws IOException
	{
		requireAlive();
		int length = receiveLength();
		byte[] data = receiveArray(length);
		String message = new String(data);
		LuaTable payload = cs2dConnetor.getLuaManager().deserialize(message).checktable();
		return cs2dConnetor.getCs2dMessageFactory().build(payload);
	}
	
	public void shutdown()
	{
		if (isAlive())
		{
			alive = false;
			try
			{
				socket.close();
			}
			catch (IOException e)
			{
				logger.error("failed to shutdown socket", e);
			}
		}
	}
}
