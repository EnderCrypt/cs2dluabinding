/************************************************************************
 * Cs2dLuaBinding by EnderCrypt                                         *
 * Copyright (C) 2022                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.library.cs2dlua.connector;


import endercrypt.library.cs2dlua.utility.UncheckedInterruptedException;

import java.util.Objects;


public class Cs2dConnectionHolder
{
	private Object lock = new Object();
	private Cs2dConnection connection = null;
	
	public void set(Cs2dConnection connection)
	{
		synchronized (lock)
		{
			this.connection = Objects.requireNonNull(connection, "connection");
			lock.notifyAll();
		}
	}
	
	public void remove()
	{
		synchronized (lock)
		{
			this.connection = null;
		}
	}
	
	public boolean exists()
	{
		return (connection != null);
	}
	
	public Cs2dConnection retrieve()
	{
		try
		{
			synchronized (lock)
			{
				while (exists() == false)
				{
					lock.wait();
				}
				return connection;
			}
		}
		catch (InterruptedException e)
		{
			throw new UncheckedInterruptedException(e);
		}
	}
}
