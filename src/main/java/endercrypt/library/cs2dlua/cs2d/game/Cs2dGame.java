/************************************************************************
 * Cs2dLuaBinding by EnderCrypt                                         *
 * Copyright (C) 2022                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.library.cs2dlua.cs2d.game;


import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;

import org.luaj.vm2.LuaValue;

import endercrypt.library.cs2dlua.cs2d.Cs2d;
import endercrypt.library.cs2dlua.cs2d.abstr.Cs2dRequestorEntity;
import endercrypt.library.cs2dlua.cs2d.abstr.requestor.Cs2dValueCacheLevel;
import endercrypt.library.cs2dlua.message.types.Cs2dLuaCallMethod;


/**
 * https://www.cs2d.com/help.php?luacat=all&luacmd=game#cmd
 * 
 * @author endercrypt
 */
public class Cs2dGame extends Cs2dRequestorEntity
{
	public Cs2dGame(Cs2d server)
	{
		super(server);
	}
	
	@Override
	protected Cs2dLuaCallMethod generate(String key)
	{
		return Cs2dLuaCallMethod.assemble("game", LuaValue.valueOf(key));
	}
	
	public String getVersion()
	{
		return fetch("version", Cs2dValueCacheLevel.FOREVER).checkjstring();
	}
	
	public boolean isDedicated()
	{
		return fetch("dedicated", Cs2dValueCacheLevel.FOREVER).checkboolean();
	}
	
	public int getPhase()
	{
		return fetch("phase").checkint();
	}
	
	public int getRound()
	{
		return fetch("round").checkint();
	}
	
	public int getTimeLeft()
	{
		return fetch("timeleft").checkint();
	}
	
	public double getMapTimeLeft()
	{
		double mapTimeLeft = fetch("maptimeleft").checkdouble();
		if (mapTimeLeft == 1000000)
		{
			return Double.POSITIVE_INFINITY;
		}
		return mapTimeLeft;
	}
	
	public int getScoreT()
	{
		return fetch("score_t").checkint();
	}
	
	public int getScoreCt()
	{
		return fetch("score_ct").checkint();
	}
	
	public int getWinRowT()
	{
		return fetch("winrow_t").checkint();
	}
	
	public int getWinRowCt()
	{
		return fetch("winrow_ct").checkint();
	}
	
	public String getNextMap()
	{
		return fetch("nextmap", Cs2dValueCacheLevel.FOREVER).checkjstring();
	}
	
	public int getTicks()
	{
		return fetch("ticks").checkint();
	}
	
	public Optional<Integer> getPort()
	{
		String port = fetch("port").checkjstring();
		
		if (port.equals("no socket"))
		{
			return Optional.empty();
		}
		
		return Optional.of(Integer.parseInt(port));
	}
	
	public boolean isBombPlanted()
	{
		return fetch("bombplanted").checkboolean();
	}
	
	public Path getSysFolder()
	{
		return Paths.get(fetch("sysfolder", Cs2dValueCacheLevel.FOREVER).checkjstring());
	}
}
