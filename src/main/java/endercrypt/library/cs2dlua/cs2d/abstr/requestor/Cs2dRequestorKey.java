/************************************************************************
 * Cs2dLuaBinding by EnderCrypt                                         *
 * Copyright (C) 2022                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.library.cs2dlua.cs2d.abstr.requestor;


import java.util.Objects;

import org.luaj.vm2.LuaValue;

import endercrypt.library.cs2dlua.cs2d.abstr.RefreshableEntity;


public class Cs2dRequestorKey implements RefreshableEntity
{
	private final Cs2dRequestor requestor;
	private final String key;
	private final Cs2dRequestorGenerator generator;
	
	private LuaValue value;
	private Cs2dValueCacheLevel cache; // use global by default
	
	public Cs2dRequestorKey(Cs2dRequestor requestor, String key, Cs2dRequestorGenerator generator)
	{
		this.requestor = requestor;
		this.key = key;
		this.generator = generator;
	}
	
	public void setCache(Cs2dValueCacheLevel cache)
	{
		this.cache = Objects.requireNonNull(cache, "cache");
	}
	
	public void setValue(LuaValue value)
	{
		this.value = Objects.requireNonNull(value, "value");
	}
	
	private Cs2dValueCacheLevel getEffectiveCacheLevel()
	{
		if (cache == null)
		{
			return requestor.getDefaultCacheLevel();
		}
		return cache;
	}
	
	@Override
	public void refresh()
	{
		if (getEffectiveCacheLevel() == Cs2dValueCacheLevel.ON_FRAME)
		{
			forceRefresh();
		}
	}
	
	@Override
	public void forceRefresh()
	{
		this.value = null;
	}
	
	public LuaValue fetch()
	{
		if (getEffectiveCacheLevel() == Cs2dValueCacheLevel.NO_CACHE)
		{
			value = null;
		}
		if (value == null)
		{
			value = request();
		}
		return Objects.requireNonNull(value, "value");
	}
	
	private LuaValue request()
	{
		return requestor.getCs2d().getConnector().getTransmitter().send(generator.generate(key));
	}
}
