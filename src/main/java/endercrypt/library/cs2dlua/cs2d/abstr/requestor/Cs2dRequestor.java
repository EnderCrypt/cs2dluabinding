/************************************************************************
 * Cs2dLuaBinding by EnderCrypt                                         *
 * Copyright (C) 2022                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.library.cs2dlua.cs2d.abstr.requestor;


import java.util.HashMap;
import java.util.Map;

import org.luaj.vm2.LuaValue;

import endercrypt.library.cs2dlua.cs2d.Cs2d;
import endercrypt.library.cs2dlua.cs2d.abstr.RefreshableEntity;


public class Cs2dRequestor implements RefreshableEntity
{
	private final Cs2d cs2d;
	private final Cs2dRequestorGenerator generator;
	
	private final Map<String, Cs2dRequestorKey> values = new HashMap<>();
	private Cs2dValueCacheLevel defaultCache = Cs2dValueCacheLevel.ON_FRAME;
	
	public Cs2dRequestor(Cs2d cs2d, Cs2dRequestorGenerator generator)
	{
		this.cs2d = cs2d;
		this.generator = generator;
	}
	
	public Cs2d getCs2d()
	{
		return cs2d;
	}
	
	public Cs2dValueCacheLevel getDefaultCacheLevel()
	{
		return defaultCache;
	}
	
	private Cs2dRequestorKey getRequestor(String key)
	{
		return values.computeIfAbsent(key, this::createRequestor);
	}
	
	private Cs2dRequestorKey createRequestor(String key)
	{
		return new Cs2dRequestorKey(this, key, generator);
	}
	
	public LuaValue fetch(String key)
	{
		return getRequestor(key).fetch();
	}
	
	public void setDefaultCacheLevel(Cs2dValueCacheLevel cache)
	{
		this.defaultCache = cache;
	}
	
	public void setCache(String key, Cs2dValueCacheLevel cache)
	{
		getRequestor(key).setCache(cache);
	}
	
	public void setValue(String key, LuaValue value)
	{
		getRequestor(key).setValue(value);
	}
	
	@Override
	public void refresh()
	{
		values.values().forEach(Cs2dRequestorKey::refresh);
	}
	
	@Override
	public void forceRefresh()
	{
		values.values().forEach(Cs2dRequestorKey::forceRefresh);
	}
}
