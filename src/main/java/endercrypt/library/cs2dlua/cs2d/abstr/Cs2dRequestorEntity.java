/************************************************************************
 * Cs2dLuaBinding by EnderCrypt                                         *
 * Copyright (C) 2022                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.library.cs2dlua.cs2d.abstr;


import org.luaj.vm2.LuaValue;

import endercrypt.library.cs2dlua.cs2d.Cs2d;
import endercrypt.library.cs2dlua.cs2d.abstr.requestor.Cs2dRequestor;
import endercrypt.library.cs2dlua.cs2d.abstr.requestor.Cs2dValueCacheLevel;
import endercrypt.library.cs2dlua.message.types.Cs2dLuaCallMethod;


public abstract class Cs2dRequestorEntity extends Cs2dContainer implements RefreshableEntity
{
	private final Cs2dRequestor requestor = new Cs2dRequestor(getCs2d(), Cs2dRequestorEntity.this::generate);
	
	protected Cs2dRequestorEntity(Cs2d cs2d)
	{
		super(cs2d);
	}
	
	protected LuaValue fetch(String key)
	{
		return requestor.fetch(key);
	}
	
	protected LuaValue fetch(String key, Cs2dValueCacheLevel cache)
	{
		LuaValue value = fetch(key);
		setDefaultCacheLevel(cache);
		return value;
	}
	
	protected abstract Cs2dLuaCallMethod generate(String key);
	
	protected void setValue(String key, LuaValue value)
	{
		requestor.setValue(key, value);
	}
	
	@Override
	public void refresh()
	{
		requestor.refresh();
	}
	
	@Override
	public void forceRefresh()
	{
		requestor.forceRefresh();
	}
	
	protected void setDefaultCacheLevel(Cs2dValueCacheLevel cache)
	{
		requestor.setDefaultCacheLevel(cache);
	}
	
	protected void setCache(String key, Cs2dValueCacheLevel cache)
	{
		requestor.setCache(key, cache);
	}
}
