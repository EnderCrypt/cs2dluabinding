/************************************************************************
 * Cs2dLuaBinding by EnderCrypt                                         *
 * Copyright (C) 2022                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.library.cs2dlua.cs2d.entities.image;


import java.awt.Dimension;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import org.luaj.vm2.LuaValue;

import endercrypt.library.cs2dlua.cs2d.Cs2d;
import endercrypt.library.cs2dlua.cs2d.abstr.Cs2dContainer;
import endercrypt.library.cs2dlua.cs2d.entities.abstr.id.Cs2dIdEntity;
import endercrypt.library.cs2dlua.cs2d.entities.image.configuration.ImageConfiguration;
import endercrypt.library.cs2dlua.cs2d.entities.image.configuration.ImageConfigurationFreeFloat;
import endercrypt.library.cs2dlua.cs2d.entities.itemtypes.Cs2dItemType;
import endercrypt.library.cs2dlua.cs2d.entities.objects.Cs2dObject;
import endercrypt.library.cs2dlua.cs2d.entities.players.Cs2dPlayer;
import endercrypt.library.cs2dlua.hook.result.collector.HookResultCollector;
import endercrypt.library.cs2dlua.hook.result.impl.object.HitZoneResult;
import endercrypt.library.cs2dlua.utility.position.PixelPosition;


public class Cs2dImage extends Cs2dContainer implements Cs2dIdEntity
{
	protected static Cs2dImage createUnsafe(Cs2d cs2d, int id, ImageConfiguration configuration)
	{
		return new Cs2dImage(cs2d, id, configuration);
	}
	
	private final int id;
	private final ImageConfiguration configuration;
	private boolean exists = true;
	
	private List<HitZoneListener> hitZoneListeners = new ArrayList<>();
	
	private Cs2dImage(Cs2d cs2d, int id, ImageConfiguration configuration)
	{
		super(cs2d);
		this.id = id;
		this.configuration = configuration;
	}
	
	@Override
	public int getId()
	{
		return id;
	}
	
	@Override
	public boolean isExists()
	{
		return exists;
	}
	
	public boolean isFreeFloatingImage()
	{
		return configuration instanceof ImageConfigurationFreeFloat;
	}
	
	private LuaValue call(String method, LuaValue... args)
	{
		if (isExists() == false)
		{
			throw new ImageAbsentException("cannot call " + method + " as this image is absent");
		}
		List<LuaValue> argList = new ArrayList<>();
		argList.add(LuaValue.valueOf(getId()));
		argList.addAll(Arrays.asList(args));
		
		LuaValue[] completeArgs = argList.stream().toArray(LuaValue[]::new);
		
		return getCs2d().getConnector().getTransmitter().call(method, completeArgs);
	}
	
	public void disableHitZone()
	{
		call("imagehitzone", LuaValue.valueOf(0));
	}
	
	public void configureHitZone(ImageHitZoneEffect effect, boolean stopShots)
	{
		configureHitZone(effect, stopShots, null);
	}
	
	public void configureHitZone(ImageHitZoneEffect effect, boolean stopShots, Dimension offset)
	{
		configureHitZone(effect, stopShots, offset, null);
	}
	
	public void configureHitZone(ImageHitZoneEffect effect, boolean stopShots, Dimension offset, Dimension size)
	{
		LuaValue luaEffect = LuaValue.valueOf((stopShots ? 100 : 0) + effect.getCode());
		LuaValue luaOffsetX = LuaValue.NIL;
		LuaValue luaOffsetY = LuaValue.NIL;
		if (offset != null)
		{
			luaOffsetX = LuaValue.valueOf(offset.width);
			luaOffsetY = LuaValue.valueOf(offset.height);
		}
		LuaValue luaScaleWidth = LuaValue.NIL;
		LuaValue luaScaleHeight = LuaValue.NIL;
		if (offset != null)
		{
			luaScaleWidth = LuaValue.valueOf(size.width);
			luaScaleHeight = LuaValue.valueOf(size.height);
		}
		call("imagehitzone", luaEffect, luaOffsetX, luaOffsetY, luaScaleWidth, luaScaleHeight);
	}
	
	public void registerHitZoneListener(HitZoneListener hitZoneListener)
	{
		Objects.requireNonNull(hitZoneListener, "hitZoneListener");
		hitZoneListeners.add(hitZoneListener);
	}
	
	protected HitZoneResult triggerHitZoneListeners(Cs2dPlayer player, Cs2dObject object, Cs2dItemType itemType, PixelPosition position, int damage)
	{
		HookResultCollector collector = new HookResultCollector();
		for (HitZoneListener listener : hitZoneListeners)
		{
			collector.merge(listener.onHitzone(player, object, itemType, position, damage));
		}
		return (HitZoneResult) collector.getResult().orElse(null);
	}
	
	public void free()
	{
		if (isExists())
		{
			call("freeimage");
			drop();
		}
	}
	
	protected void drop()
	{
		exists = false;
	}
}
