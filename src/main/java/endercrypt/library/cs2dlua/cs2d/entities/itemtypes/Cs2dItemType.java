/************************************************************************
 * Cs2dLuaBinding by EnderCrypt                                         *
 * Copyright (C) 2022                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.library.cs2dlua.cs2d.entities.itemtypes;


import java.util.Optional;

import org.luaj.vm2.LuaValue;

import endercrypt.library.cs2dlua.cs2d.Cs2d;
import endercrypt.library.cs2dlua.cs2d.abstr.Cs2dRequestorEntity;
import endercrypt.library.cs2dlua.cs2d.abstr.requestor.Cs2dValueCacheLevel;
import endercrypt.library.cs2dlua.cs2d.entities.abstr.id.Cs2dIdEntity;
import endercrypt.library.cs2dlua.message.types.Cs2dLuaCallMethod;


public class Cs2dItemType extends Cs2dRequestorEntity implements Cs2dIdEntity
{
	public static Optional<Cs2dItemType> request(Cs2d cs2d, int id)
	{
		Cs2dItemType itemType = createUnsafe(cs2d, id);
		if (itemType.isExists() == false)
		{
			return Optional.empty();
		}
		return Optional.of(itemType);
	}
	
	protected static Cs2dItemType createUnsafe(Cs2d cs2d, int id)
	{
		return new Cs2dItemType(cs2d, id);
	}
	
	private final int id;
	
	protected Cs2dItemType(Cs2d cs2d, int id)
	{
		super(cs2d);
		this.id = id;
		setDefaultCacheLevel(Cs2dValueCacheLevel.FOREVER);
	}
	
	@Override
	public int getId()
	{
		return id;
	}
	
	@Override
	protected Cs2dLuaCallMethod generate(String key)
	{
		return Cs2dLuaCallMethod.assemble("itemtype", LuaValue.valueOf(getId()), LuaValue.valueOf(key));
	}
	
	@Override
	public boolean isExists()
	{
		return getName().equals("") == false;
	}
	
	public String getName()
	{
		return fetch("name").checkjstring();
	}
	
	public int getDmg()
	{
		return fetch("dmg").checkint();
	}
	
	public int getDmgZ1()
	{
		return fetch("dmg_z1").checkint();
	}
	
	public int getDmgZ2()
	{
		return fetch("dmg_z2").checkint();
	}
	
	public int getRate()
	{
		return fetch("rate").checkint();
	}
	
	public int getReload()
	{
		return fetch("reload").checkint();
	}
	
	public int getAmmo()
	{
		return fetch("ammo").checkint();
	}
	
	public int getAmmoIn()
	{
		return fetch("ammoin").checkint();
	}
	
	public Optional<Integer> getPrice()
	{
		int price = fetch("price").checkint();
		if (price == 0)
		{
			return Optional.empty();
		}
		return Optional.of(price);
	}
	
	public int getRange()
	{
		return fetch("range").checkint();
	}
	
	public int getDispersion()
	{
		return fetch("dispersion").checkint();
	}
	
	public int getSlot()
	{
		return fetch("slot").checkint();
	}
	
	public int getRecoil()
	{
		return fetch("recoil").checkint();
	}
}
