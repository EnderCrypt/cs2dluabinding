/************************************************************************
 * Cs2dLuaBinding by EnderCrypt                                         *
 * Copyright (C) 2022                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.library.cs2dlua.cs2d.entities.objecttypes;


import org.luaj.vm2.LuaValue;

import endercrypt.library.cs2dlua.cs2d.Cs2d;
import endercrypt.library.cs2dlua.cs2d.entities.abstr.id.Cs2dIdCollection;


public class Cs2dObjectTypesCollection extends Cs2dIdCollection<Cs2dObjectType>
{
	public Cs2dObjectTypesCollection(Cs2d cs2d)
	{
		super(cs2d);
		for (int id = 0; id < 50; id++)
		{
			attemptInitBuilding(id);
		}
	}
	
	private void attemptInitBuilding(int id)
	{
		LuaValue result = getCs2d().getConnector().getTransmitter().call("objecttype", LuaValue.valueOf(id), LuaValue.valueOf("internalname"));
		if (result.checkjstring().equals("") == false)
		{
			add(new Cs2dObjectType(getCs2d(), id));
		}
	}
}
