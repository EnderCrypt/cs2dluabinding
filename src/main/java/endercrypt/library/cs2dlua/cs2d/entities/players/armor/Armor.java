/************************************************************************
 * Cs2dLuaBinding by EnderCrypt                                         *
 * Copyright (C) 2022                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.library.cs2dlua.cs2d.entities.players.armor;

public interface Armor
{
	public static Armor byCode(int code)
	{
		if (code < 0)
		{
			throw new IllegalArgumentException("code cannot be negative");
		}
		if (code == 0)
		{
			return new Unprotected();
		}
		if (code <= 200)
		{
			return new Kevlar();
		}
		if (code == 201)
		{
			return new LightArmor();
		}
		if (code == 202)
		{
			return new NormalArmor();
		}
		if (code == 203)
		{
			return new HeavyArmor();
		}
		if (code == 204)
		{
			return new MedicArmor();
		}
		if (code == 205)
		{
			return new SuperArmor();
		}
		if (code == 206)
		{
			return new StealthSuit();
		}
		throw new IllegalArgumentException("unknown armour code: " + code);
	}
	
	public abstract String getName();
	
	public abstract int getId();
	
	public abstract int getDamageReduction();
	
	public abstract int getHealing();
	
	public boolean isInvisible();
}
