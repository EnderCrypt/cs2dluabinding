/************************************************************************
 * Cs2dLuaBinding by EnderCrypt                                         *
 * Copyright (C) 2022                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.library.cs2dlua.cs2d.entities.objects;


import java.util.Optional;

import org.luaj.vm2.LuaValue;

import endercrypt.library.cs2dlua.cs2d.Cs2d;
import endercrypt.library.cs2dlua.cs2d.abstr.Cs2dRequestorEntity;
import endercrypt.library.cs2dlua.cs2d.abstr.requestor.Cs2dValueCacheLevel;
import endercrypt.library.cs2dlua.cs2d.entities.abstr.id.Cs2dIdEntity;
import endercrypt.library.cs2dlua.cs2d.entities.objecttypes.Cs2dObjectType;
import endercrypt.library.cs2dlua.cs2d.entities.players.Cs2dPlayer;
import endercrypt.library.cs2dlua.hook.data.Team;
import endercrypt.library.cs2dlua.message.types.Cs2dLuaCallMethod;
import endercrypt.library.cs2dlua.utility.position.PixelPosition;
import endercrypt.library.cs2dlua.utility.position.Position;
import endercrypt.library.cs2dlua.utility.position.TilePosition;


public class Cs2dObject extends Cs2dRequestorEntity implements Cs2dIdEntity
{
	public static Optional<Cs2dObject> request(Cs2d cs2d, int id)
	{
		Cs2dObject item = createUnsafe(cs2d, id);
		if (item.isExists() == false)
		{
			return Optional.empty();
		}
		return Optional.of(item);
	}
	
	protected static Cs2dObject createUnsafe(Cs2d cs2d, int id)
	{
		return new Cs2dObject(cs2d, id);
	}
	
	private final int id;
	
	private Cs2dObject(Cs2d cs2d, int id)
	{
		super(cs2d);
		this.id = id;
	}
	
	@Override
	public int getId()
	{
		return id;
	}
	
	@Override
	protected Cs2dLuaCallMethod generate(String key)
	{
		return Cs2dLuaCallMethod.assemble("object", LuaValue.valueOf(getId()), LuaValue.valueOf(key));
	}
	
	@Override
	public boolean isExists()
	{
		return fetch("exists").checkboolean();
	}
	
	public Cs2dObjectType getType()
	{
		int itemTypeId = fetch("type", Cs2dValueCacheLevel.FOREVER).checkint();
		return getCs2d().getObjectTypes().get(itemTypeId).get();
	}
	
	public int getHealth()
	{
		return fetch("health").checkint();
	}
	
	public int getMode()
	{
		return fetch("mode").checkint();
	}
	
	public Team getTeam()
	{
		return Team.find(fetch("team").checkint());
	}
	
	public Optional<Integer> getPlayer()
	{
		int playerId = fetch("player").checkint();
		if (playerId == 0)
		{
			return Optional.empty();
		}
		return Optional.of(playerId);
	}
	
	public PixelPosition getPosition()
	{
		int x = fetch("x").checkint();
		int y = fetch("y").checkint();
		return Position.pixel(x, y);
	}
	
	public double getRot()
	{
		return fetch("rot").checkint();
	}
	
	public double getAlpha()
	{
		return fetch("alpha").checkdouble();
	}
	
	public int getCountdown()
	{
		return fetch("countdown").checkint();
	}
	
	public int getRootRot()
	{
		return fetch("rootrot").checkint();
	}
	
	public int getIdle()
	{
		return fetch("idle").checkint();
	}
	
	public int getRotVar()
	{
		return fetch("rotvar").checkint();
	}
	
	public Optional<Cs2dPlayer> getTarget()
	{
		int playerId = fetch("target").checkint();
		if (playerId == 0)
		{
			return Optional.empty();
		}
		return getCs2d().getPlayers().get(playerId);
	}
	
	public int getUpgrade()
	{
		return fetch("upgrade").checkint();
	}
	
	public TilePosition getEntityPosition()
	{
		return Position.tile(fetch("entityx").checkint(), fetch("entityy").checkint());
	}
}
