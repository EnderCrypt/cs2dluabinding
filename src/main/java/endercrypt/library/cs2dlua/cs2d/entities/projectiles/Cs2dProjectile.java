/************************************************************************
 * Cs2dLuaBinding by EnderCrypt                                         *
 * Copyright (C) 2022                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.library.cs2dlua.cs2d.entities.projectiles;


import java.util.Optional;

import org.luaj.vm2.LuaValue;

import endercrypt.library.cs2dlua.cs2d.Cs2d;
import endercrypt.library.cs2dlua.cs2d.abstr.Cs2dRequestorEntity;
import endercrypt.library.cs2dlua.cs2d.abstr.requestor.Cs2dValueCacheLevel;
import endercrypt.library.cs2dlua.cs2d.entities.abstr.id.Cs2dIdEntity;
import endercrypt.library.cs2dlua.cs2d.entities.itemtypes.Cs2dItemType;
import endercrypt.library.cs2dlua.cs2d.entities.players.Cs2dPlayer;
import endercrypt.library.cs2dlua.message.types.Cs2dLuaCallMethod;
import endercrypt.library.cs2dlua.utility.position.PixelPosition;
import endercrypt.library.cs2dlua.utility.position.Position;


public class Cs2dProjectile extends Cs2dRequestorEntity implements Cs2dIdEntity
{
	public static Optional<Cs2dProjectile> request(Cs2d cs2d, int playerId, int id)
	{
		Cs2dProjectile item = createUnsafe(cs2d, playerId, id);
		if (item.isExists() == false)
		{
			return Optional.empty();
		}
		return Optional.of(item);
	}
	
	protected static Cs2dProjectile createUnsafe(Cs2d cs2d, int playerId, int id)
	{
		return new Cs2dProjectile(cs2d, playerId, id);
	}
	
	private final int playerId;
	private final int id;
	
	private Cs2dProjectile(Cs2d cs2d, int playerId, int id)
	{
		super(cs2d);
		this.playerId = playerId;
		this.id = id;
		
		setCache("type", Cs2dValueCacheLevel.FOREVER);
	}
	
	@Override
	public int getId()
	{
		return id;
	}
	
	public Optional<Cs2dPlayer> getPlayer()
	{
		return getCs2d().getPlayers().get(playerId);
	}
	
	@Override
	protected Cs2dLuaCallMethod generate(String key)
	{
		return Cs2dLuaCallMethod.assemble("projectile", LuaValue.valueOf(playerId), LuaValue.valueOf(getId()), LuaValue.valueOf(key));
	}
	
	@Override
	public boolean isExists()
	{
		return fetch("exists").checkboolean();
	}
	
	public Cs2dItemType getItemType()
	{
		int itemTypeId = fetch("type", Cs2dValueCacheLevel.FOREVER).checkint();
		return getCs2d().getItemTypes().get(itemTypeId).get();
	}
	
	public PixelPosition getPosition()
	{
		int x = fetch("x").checkint();
		int y = fetch("y").checkint();
		return Position.pixel(x, y);
	}
	
	public int getDirection()
	{
		return fetch("dir").checkint();
	}
	
	public int getRotation()
	{
		return fetch("rot").checkint();
	}
	
	public int getFlyDistance()
	{
		return fetch("flydist").checkint();
	}
	
	public int getTime()
	{
		return fetch("time").checkint();
	}
}
