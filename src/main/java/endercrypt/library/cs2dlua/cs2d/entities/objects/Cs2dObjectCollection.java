/************************************************************************
 * Cs2dLuaBinding by EnderCrypt                                         *
 * Copyright (C) 2022                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.library.cs2dlua.cs2d.entities.objects;


import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;

import org.luaj.vm2.LuaTable;
import org.luaj.vm2.LuaValue;

import endercrypt.library.cs2dlua.cs2d.Cs2d;
import endercrypt.library.cs2dlua.cs2d.entities.abstr.id.Cs2dDynamicIdCollection;
import endercrypt.library.cs2dlua.utility.Cs2dLuaBindUtility;


public class Cs2dObjectCollection extends Cs2dDynamicIdCollection<Cs2dObject>
{
	public Cs2dObjectCollection(Cs2d cs2d)
	{
		super(cs2d);
	}
	
	@Override
	protected Optional<Cs2dObject> tryCreate(int id)
	{
		return Cs2dObject.request(getCs2d(), id);
	}
	
	@Override
	protected Collection<Integer> fetchAllIds()
	{
		LuaTable objects = getCs2d().getConnector().getTransmitter().call("object", LuaValue.valueOf(0), LuaValue.valueOf("table")).checktable();
		return Cs2dLuaBindUtility.collectIpairs(objects).stream()
			.map(LuaValue::checkint)
			.collect(Collectors.toList());
	}
}
