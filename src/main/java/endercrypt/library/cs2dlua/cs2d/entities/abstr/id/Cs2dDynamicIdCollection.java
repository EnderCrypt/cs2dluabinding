/************************************************************************
 * Cs2dLuaBinding by EnderCrypt                                         *
 * Copyright (C) 2022                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.library.cs2dlua.cs2d.entities.abstr.id;


import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import endercrypt.library.cs2dlua.cs2d.Cs2d;


public abstract class Cs2dDynamicIdCollection<V extends Cs2dIdEntity> extends Cs2dIdCollection<V>
{
	protected Cs2dDynamicIdCollection(Cs2d cs2d)
	{
		super(cs2d);
	}
	
	protected abstract Optional<V> tryCreate(int id);
	
	@Override
	public Optional<V> get(Integer id)
	{
		V entity = super.get(id).orElse(null);
		if (entity == null)
		{
			entity = tryCreate(id).orElse(null);
			if (entity != null)
			{
				add(entity);
			}
		}
		else
		{
			if (entity.isExists() == false)
			{
				remove(id);
				entity = null;
			}
		}
		return Optional.ofNullable(entity);
	}
	
	protected abstract Collection<Integer> fetchAllIds();
	
	private void removeNonexistantEntities(Set<Integer> entityIds)
	{
		Iterator<V> iterator = map.values().iterator();
		while (iterator.hasNext())
		{
			V entity = iterator.next();
			int id = entity.getId();
			if (entityIds.contains(id) == false)
			{
				iterator.remove();
			}
		}
	}
	
	private void addExistingEntities(Set<Integer> entityIds)
	{
		for (int id : entityIds)
		{
			add(tryCreate(id).get());
		}
	}
	
	@Override
	public List<V> getAll()
	{
		Set<Integer> ids = new HashSet<>(fetchAllIds());
		removeNonexistantEntities(ids);
		addExistingEntities(ids);
		return new ArrayList<>(super.getAll());
	}
}
