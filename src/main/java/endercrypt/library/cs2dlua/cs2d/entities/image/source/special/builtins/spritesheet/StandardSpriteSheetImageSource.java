/************************************************************************
 * Cs2dLuaBinding by EnderCrypt                                         *
 * Copyright (C) 2022                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.library.cs2dlua.cs2d.entities.image.source.special.builtins.spritesheet;


import java.awt.Dimension;
import java.nio.file.Path;
import java.util.Objects;

import endercrypt.library.cs2dlua.cs2d.entities.image.source.ImageMask;


public class StandardSpriteSheetImageSource extends SpriteSheetImageSource
{
	private final Path path;
	private final Dimension tileSize;
	
	public StandardSpriteSheetImageSource(Path path, Dimension tileSize)
	{
		this.path = Objects.requireNonNull(path, "path");
		this.tileSize = Objects.requireNonNull(tileSize, "tileSize");
	}
	
	@Override
	protected String getValue()
	{
		return getPath() + ":" + getTileSize().width + ":" + getTileSize().height;
	}
	
	@Override
	public Path getPath()
	{
		return path;
	}
	
	@Override
	public Dimension getTileSize()
	{
		return tileSize;
	}
	
	public MaskedSpriteSheetImageSource withMask(ImageMask mask)
	{
		return new MaskedSpriteSheetImageSource(this, mask);
	}
}
