/************************************************************************
 * Cs2dLuaBinding by EnderCrypt                                         *
 * Copyright (C) 2022                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.library.cs2dlua.cs2d.entities.image;


import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;

import org.luaj.vm2.LuaInteger;
import org.luaj.vm2.LuaString;
import org.luaj.vm2.LuaValue;

import endercrypt.library.cs2dlua.cs2d.Cs2d;
import endercrypt.library.cs2dlua.cs2d.entities.abstr.id.Cs2dDynamicIdCollection;
import endercrypt.library.cs2dlua.cs2d.entities.image.configuration.ImageConfiguration;
import endercrypt.library.cs2dlua.cs2d.entities.image.source.ImageSource;
import endercrypt.library.cs2dlua.cs2d.entities.itemtypes.Cs2dItemType;
import endercrypt.library.cs2dlua.cs2d.entities.objects.Cs2dObject;
import endercrypt.library.cs2dlua.cs2d.entities.players.Cs2dPlayer;
import endercrypt.library.cs2dlua.hook.adapter.HookListenerAdapter;
import endercrypt.library.cs2dlua.hook.data.RoundMode;
import endercrypt.library.cs2dlua.hook.result.impl.object.HitZoneResult;
import endercrypt.library.cs2dlua.utility.position.PixelPosition;


public class Cs2dImageCollection extends Cs2dDynamicIdCollection<Cs2dImage>
{
	public Cs2dImageCollection(Cs2d cs2d)
	{
		super(cs2d);
		
		getCs2d().getConnector().getMasterListener().addPreListener(new PreListener(cs2d));
		getCs2d().getConnector().getMasterListener().addListener(new Listener(cs2d));
		getCs2d().getConnector().getMasterListener().addPostListener(new PostListener(cs2d));
	}
	
	public Cs2dImage create(ImageSource path, ImageConfiguration configuration)
	{
		return create(path, configuration, null);
	}
	
	public Cs2dImage create(ImageSource path, ImageConfiguration configuration, Cs2dPlayer visibleTo)
	{
		LuaString luaImagePath = LuaValue.valueOf(path.asKey());
		PixelPosition position = configuration.getPosition();
		LuaInteger luaX = LuaValue.valueOf(position.x);
		LuaInteger luaY = LuaValue.valueOf(position.y);
		LuaInteger luaMode = LuaValue.valueOf(configuration.getMode());
		LuaValue luaVisibleTo = visibleTo == null ? LuaValue.NIL : LuaValue.valueOf(visibleTo.getId());
		int imageId = getCs2d().getConnector().getTransmitter().call("image", luaImagePath, luaX, luaY, luaMode, luaVisibleTo).checkint();
		Cs2dImage image = Cs2dImage.createUnsafe(getCs2d(), imageId, configuration);
		add(image);
		return image;
	}
	
	@Override
	protected Optional<Cs2dImage> tryCreate(int id)
	{
		return Optional.empty();
	}
	
	@Override
	protected Collection<Integer> fetchAllIds()
	{
		return map.values().stream()
			.filter(Cs2dImage::isExists)
			.map(Cs2dImage::getId)
			.collect(Collectors.toList());
	}
	
	public class PreListener extends HookListenerAdapter
	{
		public PreListener(Cs2d cs2d)
		{
			super(cs2d);
		}
		
		@Override
		public HitZoneResult onHitZone(Cs2dImage image, Cs2dPlayer player, Cs2dObject object, Cs2dItemType itemType, PixelPosition position, int damage)
		{
			return image.triggerHitZoneListeners(player, object, itemType, position, damage);
		}
	}
	
	public class Listener extends HookListenerAdapter
	{
		public Listener(Cs2d cs2d)
		{
			super(cs2d);
		}
	}
	
	public class PostListener extends HookListenerAdapter
	{
		public PostListener(Cs2d cs2d)
		{
			super(cs2d);
		}
		
		@Override
		public void onEndRound(int delay, RoundMode mode)
		{
			getAll().forEach(Cs2dImage::drop);
		}
	}
}
