/************************************************************************
 * Cs2dLuaBinding by EnderCrypt                                         *
 * Copyright (C) 2022                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.library.cs2dlua.cs2d.entities.image.source;


import java.awt.Dimension;
import java.nio.file.Path;
import java.nio.file.Paths;

import endercrypt.library.cs2dlua.cs2d.entities.image.source.path.FileImageSourcePath;
import endercrypt.library.cs2dlua.cs2d.entities.image.source.special.builtins.AvatarImageSource;
import endercrypt.library.cs2dlua.cs2d.entities.image.source.special.builtins.FlagImageSource;
import endercrypt.library.cs2dlua.cs2d.entities.image.source.special.builtins.TileImageSource;
import endercrypt.library.cs2dlua.cs2d.entities.image.source.special.builtins.spritesheet.StandardSpriteSheetImageSource;
import endercrypt.library.cs2dlua.cs2d.entities.players.Cs2dPlayer;


public interface ImageSource
{
	public static FileImageSourcePath fromFile(String path)
	{
		return fromFile(Paths.get(path));
	}
	
	public static FileImageSourcePath fromFile(Path path)
	{
		return new FileImageSourcePath(path);
	}
	
	public static TileImageSource fromTile(int tile)
	{
		return new TileImageSource(tile);
	}
	
	public static FlagImageSource fromFlag(String flag)
	{
		return new FlagImageSource(flag);
	}
	
	public static AvatarImageSource fromAvatar(Cs2dPlayer player)
	{
		return new AvatarImageSource(player);
	}
	
	public static StandardSpriteSheetImageSource fromSpriteSheet(String path, int width, int height)
	{
		return fromSpriteSheet(Paths.get(path), width, height);
	}
	
	public static StandardSpriteSheetImageSource fromSpriteSheet(Path path, int width, int height)
	{
		return new StandardSpriteSheetImageSource(path, new Dimension(width, height));
	}
	
	public String asKey();
}
