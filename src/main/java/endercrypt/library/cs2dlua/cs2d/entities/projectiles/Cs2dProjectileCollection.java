/************************************************************************
 * Cs2dLuaBinding by EnderCrypt                                         *
 * Copyright (C) 2022                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.library.cs2dlua.cs2d.entities.projectiles;


import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;

import org.luaj.vm2.LuaTable;
import org.luaj.vm2.LuaValue;

import endercrypt.library.cs2dlua.cs2d.Cs2d;
import endercrypt.library.cs2dlua.cs2d.entities.abstr.id.Cs2dDynamicIdCollection;
import endercrypt.library.cs2dlua.cs2d.entities.players.Cs2dPlayer;
import endercrypt.library.cs2dlua.utility.Cs2dLuaBindUtility;


public class Cs2dProjectileCollection extends Cs2dDynamicIdCollection<Cs2dProjectile>
{
	private final Cs2dPlayer player;
	
	public Cs2dProjectileCollection(Cs2d cs2d, Cs2dPlayer player)
	{
		super(cs2d);
		this.player = player;
	}
	
	@Override
	protected Optional<Cs2dProjectile> tryCreate(int id)
	{
		return Cs2dProjectile.request(getCs2d(), player.getId(), id);
	}
	
	@Override
	protected Collection<Integer> fetchAllIds()
	{
		LuaTable projectiles = getCs2d().getConnector().getTransmitter().call("projectilelist", LuaValue.valueOf(0), LuaValue.valueOf(player.getId())).checktable();
		return Cs2dLuaBindUtility.collectIpairs(projectiles).stream()
			.map(value -> value.get("id"))
			.map(LuaValue::checkint)
			.collect(Collectors.toList());
	}
}
