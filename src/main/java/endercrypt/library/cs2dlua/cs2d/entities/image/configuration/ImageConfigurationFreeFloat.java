/************************************************************************
 * Cs2dLuaBinding by EnderCrypt                                         *
 * Copyright (C) 2022                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.library.cs2dlua.cs2d.entities.image.configuration;


import java.util.Objects;

import endercrypt.library.cs2dlua.utility.position.PixelPosition;
import endercrypt.library.cs2dlua.utility.position.Position;


public class ImageConfigurationFreeFloat implements ImageConfiguration
{
	private final ImageLayerMode mode;
	private final PixelPosition position;
	
	public ImageConfigurationFreeFloat(ImageLayerMode mode, Position position)
	{
		this.mode = Objects.requireNonNull(mode, "mode");
		this.position = Objects.requireNonNull(position, "position").asPixelPosition();
	}
	
	@Override
	public int getMode()
	{
		return mode.getCode();
	}
	
	@Override
	public PixelPosition getPosition()
	{
		return position;
	}
	
	public enum ImageLayerMode
	{
		floor(0),
		top(1),
		hud(2),
		superTop(3),
		background(4);
		
		private final int code;
		
		private ImageLayerMode(int code)
		{
			this.code = code;
		}
		
		public int getCode()
		{
			return code;
		}
	}
}
