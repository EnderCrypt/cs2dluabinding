/************************************************************************
 * Cs2dLuaBinding by EnderCrypt                                         *
 * Copyright (C) 2022                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.library.cs2dlua.cs2d.entities.objecttypes;


import java.util.Optional;

import org.luaj.vm2.LuaValue;

import endercrypt.library.cs2dlua.cs2d.Cs2d;
import endercrypt.library.cs2dlua.cs2d.abstr.Cs2dRequestorEntity;
import endercrypt.library.cs2dlua.cs2d.abstr.requestor.Cs2dValueCacheLevel;
import endercrypt.library.cs2dlua.cs2d.entities.abstr.id.Cs2dIdEntity;
import endercrypt.library.cs2dlua.message.types.Cs2dLuaCallMethod;


public class Cs2dObjectType extends Cs2dRequestorEntity implements Cs2dIdEntity
{
	public static Optional<Cs2dObjectType> request(Cs2d cs2d, int id)
	{
		Cs2dObjectType itemType = createUnsafe(cs2d, id);
		if (itemType.isExists() == false)
		{
			return Optional.empty();
		}
		return Optional.of(itemType);
	}
	
	protected static Cs2dObjectType createUnsafe(Cs2d cs2d, int id)
	{
		return new Cs2dObjectType(cs2d, id);
	}
	
	private final int id;
	
	protected Cs2dObjectType(Cs2d cs2d, int id)
	{
		super(cs2d);
		this.id = id;
	}
	
	@Override
	public int getId()
	{
		return id;
	}
	
	@Override
	protected Cs2dLuaCallMethod generate(String key)
	{
		return Cs2dLuaCallMethod.assemble("objecttype", LuaValue.valueOf(getId()), LuaValue.valueOf(key));
	}
	
	@Override
	public boolean isExists()
	{
		return getInternalName().equals("") == false;
	}
	
	public String getName()
	{
		return fetch("name", Cs2dValueCacheLevel.FOREVER).checkjstring();
	}
	
	public String getInternalName()
	{
		return fetch("internalname", Cs2dValueCacheLevel.FOREVER).checkjstring();
	}
	
	public ObjectType getType()
	{
		return ObjectType.find(fetch("type", Cs2dValueCacheLevel.FOREVER).checkint());
	}
	
	public int getHealth()
	{
		return fetch("health").checkint();
	}
	
	public int getKillMoney()
	{
		return fetch("killmoney").checkint();
	}
	
	public int getLimit()
	{
		return fetch("limit").checkint();
	}
	
	public int getUpgradePoints()
	{
		return fetch("upgradepoints").checkint();
	}
	
	public int getUpgradePrice()
	{
		return fetch("upgradeprice").checkint();
	}
	
	public Optional<Cs2dObjectType> getUpgradeTo()
	{
		int targetId = fetch("upgradeto").checkint();
		if (targetId == 0)
		{
			return Optional.empty();
		}
		return getCs2d().getObjectTypes().get(targetId);
	}
}
