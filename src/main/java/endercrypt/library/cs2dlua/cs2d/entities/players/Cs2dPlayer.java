/************************************************************************
 * Cs2dLuaBinding by EnderCrypt                                         *
 * Copyright (C) 2022                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.library.cs2dlua.cs2d.entities.players;


import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.luaj.vm2.LuaInteger;
import org.luaj.vm2.LuaString;
import org.luaj.vm2.LuaValue;

import endercrypt.library.cs2dlua.cs2d.Cs2d;
import endercrypt.library.cs2dlua.cs2d.abstr.Cs2dRequestorEntity;
import endercrypt.library.cs2dlua.cs2d.abstr.requestor.Cs2dValueCacheLevel;
import endercrypt.library.cs2dlua.cs2d.entities.abstr.id.Cs2dIdEntity;
import endercrypt.library.cs2dlua.cs2d.entities.players.armor.Armor;
import endercrypt.library.cs2dlua.cs2d.entities.players.properties.Language;
import endercrypt.library.cs2dlua.cs2d.entities.players.properties.identity.Steam;
import endercrypt.library.cs2dlua.cs2d.entities.players.properties.identity.Usgn;
import endercrypt.library.cs2dlua.cs2d.entities.projectiles.Cs2dProjectileCollection;
import endercrypt.library.cs2dlua.hook.data.Team;
import endercrypt.library.cs2dlua.message.types.Cs2dLuaCallMethod;
import endercrypt.library.cs2dlua.utility.MapFile;
import endercrypt.library.cs2dlua.utility.position.PixelPosition;
import endercrypt.library.cs2dlua.utility.position.Position;
import endercrypt.library.cs2dlua.utility.position.TilePosition;


/**
 * https://www.cs2d.com/help.php?luacat=all&luacmd=player#cmd
 * 
 * @author endercrypt
 */
public class Cs2dPlayer extends Cs2dRequestorEntity implements Cs2dIdEntity
{
	public static Optional<Cs2dPlayer> request(Cs2d cs2d, int id)
	{
		Cs2dPlayer player = createUnsafe(cs2d, id);
		if (player.isExists() == false)
		{
			return Optional.empty();
		}
		return Optional.of(player);
	}
	
	protected static Cs2dPlayer createUnsafe(Cs2d cs2d, int id)
	{
		return new Cs2dPlayer(cs2d, id);
	}
	
	private final int id;
	private Cs2dProjectileCollection projectileController = new Cs2dProjectileCollection(getCs2d(), this);
	
	protected Cs2dPlayer(Cs2d server, int id)
	{
		super(server);
		this.id = id;
	}
	
	@Override
	public int getId()
	{
		return id;
	}
	
	@Override
	protected Cs2dLuaCallMethod generate(String key)
	{
		LuaInteger luaId = LuaValue.valueOf(getId());
		LuaString luaKey = LuaValue.valueOf(key);
		return Cs2dLuaCallMethod.assemble("player", luaId, luaKey);
	}
	
	// Identity & Logins & Language
	
	@Override
	public boolean isExists()
	{
		return fetch("exists").checkboolean();
	}
	
	public String getName()
	{
		return fetch("name").checkjstring();
	}
	
	public String getIp()
	{
		return fetch("ip", Cs2dValueCacheLevel.FOREVER).checkjstring();
	}
	
	public int getPort()
	{
		return fetch("port", Cs2dValueCacheLevel.FOREVER).checkint();
	}
	
	public Usgn getUsgn()
	{
		return new Usgn(fetch("usgn", Cs2dValueCacheLevel.FOREVER).checkint(), fetch("usgnname", Cs2dValueCacheLevel.FOREVER).checkjstring());
	}
	
	public Steam getSteam()
	{
		return new Steam(fetch("steamid", Cs2dValueCacheLevel.FOREVER).checkjstring(), fetch("steamname", Cs2dValueCacheLevel.FOREVER).checkjstring());
	}
	
	public boolean isBot()
	{
		return fetch("bot", Cs2dValueCacheLevel.FOREVER).checkboolean();
	}
	
	public boolean isRcon()
	{
		return fetch("rcon").checkboolean();
	}
	
	public Language getLanguage()
	{
		return new Language(fetch("language", Cs2dValueCacheLevel.FOREVER).checkjstring(), fetch("language_iso", Cs2dValueCacheLevel.FOREVER).checkjstring());
	}
	
	// Team & Appearance
	
	public Team getTeam()
	{
		return Team.find(fetch("team").checkint());
	}
	
	public Team getFavTeam()
	{
		return Team.find(fetch("favteam").checkint());
	}
	
	public int getLook()
	{
		return fetch("look").checkint();
	}
	
	public String getSprayName()
	{
		return fetch("sprayname").checkjstring();
	}
	
	public int getSprayColor()
	{
		return fetch("spraycolor").checkint();
	}
	
	// Mouse Position & Screen/Setup
	
	public Optional<PixelPosition> getMouse()
	{
		if (isDead())
		{
			return Optional.empty();
		}
		return Optional.of(Position.pixel(fetch("mousex").checkint(), fetch("mousey").checkint()));
	}
	
	public Optional<PixelPosition> getMapMouse()
	{
		if (isDead())
		{
			return Optional.empty();
		}
		return Optional.of(Position.pixel(fetch("mousemapx").checkint(), fetch("mousemapy").checkint()));
	}
	
	public Optional<Double> getMouseDistance()
	{
		if (isDead())
		{
			return Optional.empty();
		}
		return Optional.of(fetch("mousedist").checkdouble());
	}
	
	public PixelPosition getScreen()
	{
		return Position.pixel(fetch("screenw").checkint(), fetch("screenh").checkint());
	}
	
	public boolean isWideScreen()
	{
		return fetch("widescreen").checkint() == 1;
	}
	
	public boolean isWindowed()
	{
		return fetch("windowed").checkint() == 1;
	}
	
	public boolean isMicSupport()
	{
		return fetch("micsupport").checkint() == 1;
	}
	
	// Position
	
	public PixelPosition getPixelPosition()
	{
		return Position.pixel(fetch("x").checkint(), fetch("y").checkint());
	}
	
	public TilePosition getTilePosition()
	{
		return Position.tile(fetch("tilex").checkint(), fetch("tiley").checkint());
	}
	
	public double getRotation()
	{
		return fetch("rot").checkdouble();
	}
	
	// Stats
	
	public int getHealth()
	{
		return fetch("health").checkint();
	}
	
	public boolean isAlive()
	{
		return getHealth() > 0;
	}
	
	public boolean isDead()
	{
		return getHealth() <= 0;
	}
	
	public int getMaxHealth()
	{
		return fetch("maxhealth").checkint();
	}
	
	public Armor getArmour()
	{
		return Armor.byCode(fetch("armor").checkint());
	}
	
	public int getMoney()
	{
		return fetch("money").checkint();
	}
	
	public int getScore()
	{
		return fetch("score").checkint();
	}
	
	public int getDeaths()
	{
		return fetch("deaths").checkint();
	}
	
	public int getTeamKills()
	{
		return fetch("teamkills").checkint();
	}
	
	public int getHostageKills()
	{
		return fetch("hostagekills").checkint();
	}
	
	public int getTeamBuildingKills()
	{
		return fetch("teambuildingkills").checkint();
	}
	
	public int getMvpRounds()
	{
		return fetch("mvp").checkint();
	}
	
	public int getAssists()
	{
		return fetch("assists").checkint();
	}
	
	public int getPingMs()
	{
		return fetch("ping").checkint();
	}
	
	public int getIdleSeconds()
	{
		return fetch("idle").checkint();
	}
	
	public int getSpeedmod()
	{
		return fetch("speedmod").checkint();
	}
	
	public Optional<Cs2dPlayer> getSpectating()
	{
		int spectatingId = fetch("spectating").checkint();
		return getCs2d().getPlayers().get(spectatingId);
	}
	
	public List<Cs2dPlayer> getSpectators()
	{
		return getCs2d().getPlayers().find(p -> {
			
			Cs2dPlayer spect = p.getSpectating().orElse(null);
			return (spect != null && spect.equals(Cs2dPlayer.this));
			
		}).collect(Collectors.toList());
	}
	
	public int getAiBotFlashed()
	{
		return fetch("ai_flashed").checkint();
	}
	
	// Equipment
	
	public int getWeaponType()
	{
		return fetch("weapontype").checkint();
	}
	
	public int getWeaponMode()
	{
		return fetch("weaponmode").checkint();
	}
	
	public boolean hasNightVision()
	{
		return fetch("nightvision").checkboolean();
	}
	
	public boolean hasDefuseKit()
	{
		return fetch("defusekit").checkboolean();
	}
	
	public boolean hasGasMask()
	{
		return fetch("gasmask").checkboolean();
	}
	
	public boolean hasBomb()
	{
		return fetch("bomb").checkboolean();
	}
	
	public boolean hasFlag()
	{
		return fetch("flag").checkboolean();
	}
	
	// Actions & Voting
	
	public boolean isReloading()
	{
		return fetch("reloading").checkboolean();
	}
	
	public int getProcess()
	{
		return fetch("process").checkint();
	}
	
	public Optional<Cs2dPlayer> getVoteKick()
	{
		int votekickId = fetch("votekick").checkint();
		if (votekickId == 0)
		{
			return Optional.empty();
		}
		return getCs2d().getPlayers().get(votekickId);
	}
	
	public Optional<MapFile> getVoteMap()
	{
		String mapName = fetch("votemap").checkjstring();
		if (mapName.equals(""))
		{
			return Optional.empty();
		}
		return Optional.of(new MapFile(mapName));
	}
	
	// EXTRA
	
	@Override
	public void refresh()
	{
		getProjectiles().refresh();
		super.refresh();
	}
	
	@Override
	public void forceRefresh()
	{
		getProjectiles().forceRefresh();
		super.forceRefresh();
	}
	
	public Cs2dProjectileCollection getProjectiles()
	{
		return projectileController;
	}
	
	// COMMANDS
	
	public void message(String message)
	{
		getCs2d().getConnector().getTransmitter().call("msg2", LuaValue.valueOf(getId()), LuaValue.valueOf(message));
	}
	
	public void setDeaths(int score)
	{
		getCs2d().getConnector().getTransmitter().parse("setdeaths " + getId() + " " + score);
	}
	
	public void setHealth(int health)
	{
		getCs2d().getConnector().getTransmitter().parse("sethealth " + getId() + " " + health);
	}
	
	public void setMaxHealth(int maxHealth)
	{
		getCs2d().getConnector().getTransmitter().parse("setmaxhealth " + getId() + " " + maxHealth);
	}
	
	public void setMoney(int money)
	{
		getCs2d().getConnector().getTransmitter().parse("setmoney " + getId() + " " + money);
	}
	
	public void setName(String name)
	{
		getCs2d().getConnector().getTransmitter().parse("setname " + getId() + " " + name);
	}
	
	public void setPosition(Position position)
	{
		PixelPosition pixel = position.asPixelPosition();
		getCs2d().getConnector().getTransmitter().parse("setpos " + getId() + " " + pixel.x + " " + pixel.y);
	}
	
	public void setScore(int score)
	{
		getCs2d().getConnector().getTransmitter().parse("setscore " + getId() + " " + score);
	}
	
	public void speedmod(int value)
	{
		if (value < -100 || value > 100)
		{
			throw new IllegalArgumentException("speedmod cannot be " + value);
		}
		getCs2d().getConnector().getTransmitter().parse("speedmod " + getId() + " " + value);
	}
	
	public void kick()
	{
		getCs2d().getConnector().getTransmitter().parse("kick " + getId());
	}
	
	public void kick(String reason)
	{
		getCs2d().getConnector().getTransmitter().parse("kick " + getId() + " \"" + reason + "\"");
	}
}
