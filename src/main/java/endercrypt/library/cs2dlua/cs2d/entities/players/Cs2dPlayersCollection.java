/************************************************************************
 * Cs2dLuaBinding by EnderCrypt                                         *
 * Copyright (C) 2022                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.library.cs2dlua.cs2d.entities.players;


import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.luaj.vm2.LuaTable;
import org.luaj.vm2.LuaValue;

import endercrypt.library.cs2dlua.cs2d.Cs2d;
import endercrypt.library.cs2dlua.cs2d.entities.abstr.id.Cs2dDynamicIdCollection;
import endercrypt.library.cs2dlua.utility.Cs2dLuaBindUtility;


public class Cs2dPlayersCollection extends Cs2dDynamicIdCollection<Cs2dPlayer>
{
	public Cs2dPlayersCollection(Cs2d cs2d)
	{
		super(cs2d);
	}
	
	@Override
	protected Optional<Cs2dPlayer> tryCreate(int id)
	{
		return Cs2dPlayer.request(getCs2d(), id);
	}
	
	@Override
	protected Collection<Integer> fetchAllIds()
	{
		LuaTable players = getCs2d().getConnector().getTransmitter().call("player", LuaValue.valueOf(0), LuaValue.valueOf("table")).checktable();
		return Cs2dLuaBindUtility.collectIpairs(players).stream()
			.map(LuaValue::checkint)
			.collect(Collectors.toList());
	}
	
	public Stream<Cs2dPlayer> find(Predicate<Cs2dPlayer> condition)
	{
		return getAll().stream().filter(condition);
	}
	
	public List<Cs2dPlayer> getLiving()
	{
		return find(Cs2dPlayer::isAlive).collect(Collectors.toList());
	}
	
	public List<Cs2dPlayer> getDead()
	{
		return find(Cs2dPlayer::isDead).collect(Collectors.toList());
	}
	
}
