/************************************************************************
 * Cs2dLuaBinding by EnderCrypt                                         *
 * Copyright (C) 2022                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.library.cs2dlua.cs2d.entities.image.configuration;


import java.util.Objects;

import endercrypt.library.cs2dlua.cs2d.entities.players.Cs2dPlayer;
import endercrypt.library.cs2dlua.utility.position.PixelPosition;
import endercrypt.library.cs2dlua.utility.position.Position;


public class ImageConfigurationFollowPlayer implements ImageConfiguration
{
	private final ImagePlayerMode mode;
	private final Cs2dPlayer player;
	private final ImagePlayerRotate rotate;
	private final boolean hideInFog;
	
	public ImageConfigurationFollowPlayer(ImagePlayerMode mode, Cs2dPlayer player, ImagePlayerRotate rotate, boolean hideInFog)
	{
		this.mode = Objects.requireNonNull(mode, "mode");
		this.player = Objects.requireNonNull(player, "player");
		this.rotate = Objects.requireNonNull(rotate, "rotate");
		this.hideInFog = hideInFog;
	}
	
	@Override
	public int getMode()
	{
		return mode.getCode() + player.getId();
	}
	
	@Override
	public PixelPosition getPosition()
	{
		return Position.pixel(rotate.getCode(), hideInFog ? 0 : 1);
	}
	
	public enum ImagePlayerMode
	{
		belowPlayer(100),
		abovePlayer(200),
		abovePlayerAndEntities(132);
		
		private final int code;
		
		private ImagePlayerMode(int code)
		{
			this.code = code;
		}
		
		public int getCode()
		{
			return code;
		}
	}
	
	public enum ImagePlayerRotate
	{
		doNotRotate(0),
		rotateWithPlayer(1),
		rotateWithPlayerAndWiggle(2),
		rotateWithPlayerAndWiggleAndRecoil(3);
		
		private final int code;
		
		private ImagePlayerRotate(int code)
		{
			this.code = code;
		}
		
		public int getCode()
		{
			return code;
		}
	}
}
