/************************************************************************
 * Cs2dLuaBinding by EnderCrypt                                         *
 * Copyright (C) 2022                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.library.cs2dlua.cs2d.entities.abstr.entity;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import endercrypt.library.cs2dlua.cs2d.Cs2d;
import endercrypt.library.cs2dlua.cs2d.abstr.Cs2dContainer;
import endercrypt.library.cs2dlua.cs2d.abstr.RefreshableEntity;


public abstract class Cs2dCollection<K, V extends Cs2dEntity> extends Cs2dContainer implements RefreshableEntity
{
	protected final Map<K, V> map = new HashMap<>();
	
	public Cs2dCollection(Cs2d cs2d)
	{
		super(cs2d);
	}
	
	protected abstract K getId(V entity);
	
	public void add(V entity)
	{
		map.put(getId(entity), entity);
	}
	
	public void remove(V entity)
	{
		remove(getId(entity));
	}
	
	public void remove(K id)
	{
		map.remove(id);
	}
	
	@Override
	public void refresh()
	{
		map.values().forEach((p) -> {
			if (p instanceof RefreshableEntity)
			{
				((RefreshableEntity) p).refresh();
			}
		});
	}
	
	@Override
	public void forceRefresh()
	{
		map.values().forEach((p) -> {
			if (p instanceof RefreshableEntity)
			{
				((RefreshableEntity) p).forceRefresh();
			}
		});
	}
	
	public Optional<V> get(K id)
	{
		return Optional.ofNullable(map.get(id));
	}
	
	public List<V> getAll()
	{
		return new ArrayList<>(map.values());
	}
}
