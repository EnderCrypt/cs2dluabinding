/************************************************************************
 * Cs2dLuaBinding by EnderCrypt                                         *
 * Copyright (C) 2022                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.library.cs2dlua.cs2d;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.luaj.vm2.LuaValue;

import endercrypt.library.cs2dlua.connector.Cs2dConnector;
import endercrypt.library.cs2dlua.cs2d.entities.image.Cs2dImageCollection;
import endercrypt.library.cs2dlua.cs2d.entities.items.Cs2dItemCollection;
import endercrypt.library.cs2dlua.cs2d.entities.itemtypes.Cs2dItemTypeCollection;
import endercrypt.library.cs2dlua.cs2d.entities.objects.Cs2dObjectCollection;
import endercrypt.library.cs2dlua.cs2d.entities.objecttypes.Cs2dObjectTypesCollection;
import endercrypt.library.cs2dlua.cs2d.entities.players.Cs2dPlayersCollection;
import endercrypt.library.cs2dlua.cs2d.game.Cs2dGame;
import endercrypt.library.cs2dlua.cs2d.map.Cs2dMap;
import endercrypt.library.cs2dlua.hook.mapper.HookListenerMapper;


public class Cs2d
{
	private static final Logger logger = LogManager.getLogger(Cs2d.class);
	
	private final Cs2dConnector connector;
	private final Cs2dMap map;
	private final Cs2dGame game;
	private final Cs2dPlayersCollection players;
	
	private final Cs2dItemTypeCollection itemTypes;
	private final Cs2dItemCollection items;
	
	private final Cs2dObjectTypesCollection objectTypes;
	private final Cs2dObjectCollection objects;
	
	private final Cs2dImageCollection images;
	
	public Cs2d(Cs2dConnector connector)
	{
		this.connector = connector;
		
		this.map = new Cs2dMap(this);
		this.game = new Cs2dGame(this);
		this.players = new Cs2dPlayersCollection(this);
		
		this.itemTypes = new Cs2dItemTypeCollection(this);
		this.items = new Cs2dItemCollection(this);
		
		this.objectTypes = new Cs2dObjectTypesCollection(this);
		this.objects = new Cs2dObjectCollection(this);
		
		this.images = new Cs2dImageCollection(this);
		
		connector.getMasterListener().addPreListener(new PreListener());
		connector.getMasterListener().addPostListener(new PostListener());
	}
	
	// world
	
	public Cs2dConnector getConnector()
	{
		return connector;
	}
	
	public Cs2dMap getMap()
	{
		return map;
	}
	
	public Cs2dGame getGame()
	{
		return game;
	}
	
	public Cs2dPlayersCollection getPlayers()
	{
		return players;
	}
	
	public Cs2dItemTypeCollection getItemTypes()
	{
		return itemTypes;
	}
	
	public Cs2dItemCollection getItems()
	{
		return items;
	}
	
	public Cs2dObjectTypesCollection getObjectTypes()
	{
		return objectTypes;
	}
	
	public Cs2dObjectCollection getObjects()
	{
		return objects;
	}
	
	public Cs2dImageCollection getImages()
	{
		return images;
	}
	
	// methods
	
	public void message(String message)
	{
		connector.getTransmitter().call("msg", LuaValue.valueOf(message));
	}
	
	// listeners
	
	private class PreListener extends HookListenerMapper
	{
		@Override
		public void onAlways()
		{
			map.refresh();
			game.refresh();
			players.refresh();
			
			itemTypes.refresh();
			items.refresh();
			
			objectTypes.refresh();
			objects.refresh();
			
			images.refresh();
		}
	}
	
	private class PostListener extends HookListenerMapper
	{
		@Override
		public void onMapChange(String mapName)
		{
			logger.info("Perfoming force refresh of local entities");
			map.forceRefresh();
			game.forceRefresh();
			players.forceRefresh();
			
			itemTypes.forceRefresh();
			items.forceRefresh();
			
			objectTypes.forceRefresh();
			objects.forceRefresh();
			
			images.forceRefresh();
		}
	}
}
