/************************************************************************
 * Cs2dLuaBinding by EnderCrypt                                         *
 * Copyright (C) 2022                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.library.cs2dlua.cs2d.map.entities;


import java.util.Optional;

import org.luaj.vm2.LuaValue;

import endercrypt.library.cs2dlua.cs2d.Cs2d;
import endercrypt.library.cs2dlua.cs2d.abstr.Cs2dRequestorEntity;
import endercrypt.library.cs2dlua.cs2d.abstr.requestor.Cs2dValueCacheLevel;
import endercrypt.library.cs2dlua.cs2d.entities.abstr.tile.Cs2dTileEntity;
import endercrypt.library.cs2dlua.message.types.Cs2dLuaCallMethod;
import endercrypt.library.cs2dlua.utility.position.TilePosition;


public class Cs2dMapEntity extends Cs2dRequestorEntity implements Cs2dTileEntity
{
	public static Optional<Cs2dMapEntity> request(Cs2d cs2d, TilePosition position)
	{
		Cs2dMapEntity itemType = createUnsafe(cs2d, position);
		if (itemType.isExists() == false)
		{
			return Optional.empty();
		}
		return Optional.of(itemType);
	}
	
	protected static Cs2dMapEntity createUnsafe(Cs2d cs2d, TilePosition position)
	{
		return new Cs2dMapEntity(cs2d, position);
	}
	
	private final TilePosition position;
	
	protected Cs2dMapEntity(Cs2d cs2d, TilePosition position)
	{
		super(cs2d);
		this.position = position;
		setDefaultCacheLevel(Cs2dValueCacheLevel.FOREVER);
	}
	
	@Override
	protected Cs2dLuaCallMethod generate(String key)
	{
		return Cs2dLuaCallMethod.assemble("entity", LuaValue.valueOf(position.x), LuaValue.valueOf(position.y), LuaValue.valueOf(key));
	}
	
	@Override
	public boolean isExists()
	{
		return fetch("exists").checkboolean();
	}
	
	@Override
	public TilePosition getPosition()
	{
		return position;
	}
	
	public String getTypeName()
	{
		return fetch("typename").checkjstring();
	}
	
	public int getTypeId()
	{
		return fetch("type").checkint();
	}
	
	public String getName()
	{
		return fetch("name").checkjstring();
	}
	
	public String getTrigger()
	{
		return fetch("trigger").checkjstring();
	}
	
	public LuaValue getState()
	{
		return fetch("state");
	}
	
	private LuaValue getValue(String type, int index)
	{
		if (type.equals("str") == false || type.equals("int") == false)
		{
			throw new IllegalArgumentException("type cannot be " + index + " (must be str or int)");
		}
		if (index < 0 || index > 9)
		{
			throw new IllegalArgumentException("index cannot be " + index + " (must be 0 - 9)");
		}
		String key = type + index;
		return fetch(key);
	}
	
	public String getStr(int index)
	{
		return getValue("str", index).checkjstring();
	}
	
	public int getInt(int index)
	{
		return getValue("int", index).checkint();
	}
	
	public int getAiState()
	{
		return fetch("aistate").checkint();
	}
	
	public void setAiState(int state)
	{
		LuaValue value = LuaValue.valueOf(state);
		Cs2dLuaCallMethod luaCall = Cs2dLuaCallMethod.assemble("setentityaistate", LuaValue.valueOf(position.x), LuaValue.valueOf(position.y), value);
		getCs2d().getConnector().getTransmitter().send(luaCall);
		setValue("aistate", value);
	}
}
