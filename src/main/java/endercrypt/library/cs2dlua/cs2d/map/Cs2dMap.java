/************************************************************************
 * Cs2dLuaBinding by EnderCrypt                                         *
 * Copyright (C) 2022                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.library.cs2dlua.cs2d.map;


import java.awt.Color;

import org.luaj.vm2.LuaValue;

import endercrypt.library.cs2dlua.cs2d.Cs2d;
import endercrypt.library.cs2dlua.cs2d.abstr.Cs2dRequestorEntity;
import endercrypt.library.cs2dlua.cs2d.abstr.requestor.Cs2dValueCacheLevel;
import endercrypt.library.cs2dlua.cs2d.map.entities.Cs2dMapEntityCollection;
import endercrypt.library.cs2dlua.message.types.Cs2dLuaCallMethod;
import endercrypt.library.cs2dlua.utility.position.Position;
import endercrypt.library.cs2dlua.utility.position.TilePosition;


public class Cs2dMap extends Cs2dRequestorEntity
{
	private final Cs2dMapEntityCollection entities;
	
	public Cs2dMap(Cs2d cs2d)
	{
		super(cs2d);
		setDefaultCacheLevel(Cs2dValueCacheLevel.FOREVER);
		
		this.entities = new Cs2dMapEntityCollection(cs2d);
	}
	
	public Cs2dMapEntityCollection getEntities()
	{
		return entities;
	}
	
	@Override
	protected Cs2dLuaCallMethod generate(String key)
	{
		return Cs2dLuaCallMethod.assemble("map", LuaValue.valueOf(key));
	}
	
	public String getName()
	{
		return fetch("name").checkjstring();
	}
	
	public TilePosition getSize()
	{
		int x = fetch("xsize").checkint();
		int y = fetch("ysize").checkint();
		return Position.tile(x, y);
	}
	
	public String getTileSet()
	{
		return fetch("tileset").checkjstring();
	}
	
	public int getTileSize()
	{
		return fetch("tilesize").checkint();
	}
	
	public int getTileCount()
	{
		return fetch("tilecount").checkint();
	}
	
	public int getBackgroundImage()
	{
		return fetch("back_img").checkint();
	}
	
	public TilePosition getBackgroundScroll()
	{
		int x = fetch("back_scrollx").checkint();
		int y = fetch("back_scrolly").checkint();
		return Position.tile(x, y);
	}
	
	public boolean isBackgroundScrollTiles()
	{
		return fetch("back_scrolltile").checkint() == 1;
	}
	
	public Color getBackgroundColor()
	{
		int r = fetch("back_r").checkint();
		int g = fetch("back_g").checkint();
		int b = fetch("back_b").checkint();
		return new Color(r, g, b);
	}
	
	public TilePosition getStorm()
	{
		int x = fetch("storm_x").checkint();
		int y = fetch("storm_y").checkint();
		return Position.tile(x, y);
	}
	
	public int getMissionVips()
	{
		return fetch("mission_vips").checkint();
	}
	
	public int getMissionHostages()
	{
		return fetch("mission_hostages").checkint();
	}
	
	public int getMissionBombSpots()
	{
		return fetch("mission_bombspots").checkint();
	}
	
	public int getMissionCtfFlas()
	{
		return fetch("mission_ctfflags").checkint();
	}
	
	public int getMissionDomPoints()
	{
		return fetch("mission_dompoints").checkint();
	}
	
	public boolean isBuyingAllowed()
	{
		return fetch("nobuying").checkint() == 0; // needs testing, 0 or 1
	}
	
	public boolean isWeaponsAllowed()
	{
		return fetch("noweapons").checkint() == 0; // needs testing, 0 or 1
	}
	
	public boolean hasTeleporters()
	{
		return fetch("teleporters").checkint() == 1;
	}
	
	public int getBotNodes()
	{
		return fetch("botnodes").checkint();
	}
}
