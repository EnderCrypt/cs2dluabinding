/************************************************************************
 * Cs2dLuaBinding by EnderCrypt                                         *
 * Copyright (C) 2022                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.library.cs2dlua.cs2d.map.entities;


import org.luaj.vm2.LuaTable;
import org.luaj.vm2.LuaValue;

import endercrypt.library.cs2dlua.cs2d.Cs2d;
import endercrypt.library.cs2dlua.cs2d.entities.abstr.tile.Cs2dTileCollection;
import endercrypt.library.cs2dlua.utility.Cs2dLuaBindUtility;
import endercrypt.library.cs2dlua.utility.position.Position;
import endercrypt.library.cs2dlua.utility.position.TilePosition;


public class Cs2dMapEntityCollection extends Cs2dTileCollection<Cs2dMapEntity>
{
	public Cs2dMapEntityCollection(Cs2d cs2d)
	{
		super(cs2d);
		
		LuaTable entitiesTable = getCs2d().getConnector().getTransmitter().call("entitylist").checktable();
		for (LuaValue entry : Cs2dLuaBindUtility.collectIpairs(entitiesTable))
		{
			int x = entry.get("x").checkint();
			int y = entry.get("y").checkint();
			TilePosition position = Position.tile(x, y);
			add(Cs2dMapEntity.createUnsafe(cs2d, position));
		}
	}
}
