/************************************************************************
 * Cs2dLuaBinding by EnderCrypt                                         *
 * Copyright (C) 2022                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.library.cs2dlua.lua;


import endercrypt.library.commons.data.DataSource;

import java.io.IOException;

import org.luaj.vm2.Globals;
import org.luaj.vm2.LuaFunction;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.lib.jse.JsePlatform;


public class LuaManager
{
	private final Globals enviroment = JsePlatform.debugGlobals();
	
	private final LuaFunction pickle;
	private final LuaFunction unpickle;
	
	public LuaManager() throws IOException
	{
		execute(DataSource.RESOURCES, "./pickle.lua");
		pickle = enviroment.get("pickle").checkfunction();
		unpickle = enviroment.get("unpickle").checkfunction();
	}
	
	public LuaValue execute(DataSource source, String filename) throws IOException
	{
		String script = source.read(filename).asString();
		LuaValue chunk = enviroment.load(script, filename);
		return chunk.call();
	}
	
	public String serialize(LuaValue lua)
	{
		return pickle.call(lua).checkjstring();
	}
	
	public LuaValue deserialize(String lua)
	{
		return unpickle.call(lua);
	}
}
