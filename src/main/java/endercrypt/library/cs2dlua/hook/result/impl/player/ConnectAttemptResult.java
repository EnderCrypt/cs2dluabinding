/************************************************************************
 * Cs2dLuaBinding by EnderCrypt                                         *
 * Copyright (C) 2022                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.library.cs2dlua.hook.result.impl.player;


import org.luaj.vm2.LuaValue;

import endercrypt.library.cs2dlua.hook.result.collector.SmartHookResult;


public class ConnectAttemptResult extends SmartHookResult<ConnectAttemptResult>
{
	public static ConnectAttemptResult cancel(String banReason)
	{
		return new ConnectAttemptResult(0, banReason);
	}
	
	public static ConnectAttemptResult cancel(int priority, String banReason)
	{
		return new ConnectAttemptResult(priority, banReason);
	}
	
	private int priority;
	private String banReason;
	
	private ConnectAttemptResult(int priority, String banReason)
	{
		this.priority = priority;
		this.banReason = banReason;
	}
	
	@Override
	public LuaValue generate()
	{
		if (banReason == null)
		{
			return LuaValue.valueOf("");
		}
		return LuaValue.valueOf(banReason);
	}
	
	@Override
	protected void merge(ConnectAttemptResult other)
	{
		if (banReason == null || other.banReason != null)
		{
			if (banReason == null || other.priority > priority)
			{
				banReason = other.banReason;
			}
		}
	}
}
