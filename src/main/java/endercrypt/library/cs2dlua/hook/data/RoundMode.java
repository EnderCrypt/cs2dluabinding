/************************************************************************
 * Cs2dLuaBinding by EnderCrypt                                         *
 * Copyright (C) 2022                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.library.cs2dlua.hook.data;

public enum RoundMode
{
	terroristWin(1), //(elimination)
	counterTerroristWin(2), //(elimination)
	roundDraw(3),
	gameCommencing(4),
	roundRestart(5),
	vipKilled(10), //(as_ maps)
	vipDidNotEscape(12), //(as_ maps)
	bombDetonated(20), //(de_ maps)
	hostagesProtected(30), //(cs_ maps)
	blueFlagCaptured(40), //(ctf_ maps)
	allPointsDominatedByTerrorists(50), //(dom_ maps)
	allHumansKilled(60), //(Zombies! game mode)
	vipEscaped(11), //(as_ maps)
	bombDefused(21), //(de_ maps)
	bombsiteProtected(22), //(de_ maps)
	hostagesRescued(31), //(cs_ maps)
	redFlagCaptured(41), //(ctf_ maps)
	allPointsDominatedByCounterTerrorists(51), //(dom_ maps)
	survivorsSurvived(61); //(Zombies! game mode)
	
	private final int code;
	
	private RoundMode(int code)
	{
		this.code = code;
	}
	
	public int getCode()
	{
		return code;
	}
	
	public static RoundMode find(int code)
	{
		for (RoundMode reason : values())
		{
			if (reason.getCode() == code)
			{
				return reason;
			}
		}
		throw new IllegalArgumentException("unknown code: " + code);
	}
}
