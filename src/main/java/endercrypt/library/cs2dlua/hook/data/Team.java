/************************************************************************
 * Cs2dLuaBinding by EnderCrypt                                         *
 * Copyright (C) 2022                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.library.cs2dlua.hook.data;

public enum Team
{
	Spectator(0),
	Terrorist(1),
	CounterTerrorist(2),
	Vip(3);
	
	private final int code;
	
	private Team(int code)
	{
		this.code = code;
	}
	
	public int getCode()
	{
		return code;
	}
	
	public static Team find(int code)
	{
		for (Team team : values())
		{
			if (team.getCode() == code)
			{
				return team;
			}
		}
		throw new IllegalArgumentException("unknown team code: " + code);
	}
}
