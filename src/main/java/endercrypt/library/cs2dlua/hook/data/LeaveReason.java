/************************************************************************
 * Cs2dLuaBinding by EnderCrypt                                         *
 * Copyright (C) 2022                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.library.cs2dlua.hook.data;

public enum LeaveReason
{
	normalLeave(0), // player left game manually
	pingTimeout(1),
	picked(2), // without reason text
	kickedBecauseCheatWasDetected(3),
	serverShutdown(4),
	clientConfirmedServerShutdown(5), // each client sends this when the server is being shut down
	banned(6),
	errorDuringJoinProcess(7),
	clientFailedToLoadMap(8),
	joinProcessCancelledByUser(9),
	pingTooHigh(10), // mp_pinglimit
	kickedForTooManyTeamKills(11), // mp_teamkillpenalty
	kickedForTooManyHostageKills(12), // mp_hostagepenalty
	kickedViaVotingOfOtherPlayers(13), // mp_kickpercent, vote
	kickedForReservationSystem(14), // mp_reservations
	kickedBecauseSpeedhackWasDetected(15),
	kicked(16), // with reason text
	playerHasBeenReroutedToAnotherServer(17), // reroute
	kickedForBeingIdle(18), // mp_idletime, mp_idlekick, mp_idleaction
	kickedForFloodingChatWithMessages(19);
	
	private final int code;
	
	private LeaveReason(int code)
	{
		this.code = code;
	}
	
	public int getCode()
	{
		return code;
	}
	
	public static LeaveReason find(int code)
	{
		for (LeaveReason reason : values())
		{
			if (reason.getCode() == code)
			{
				return reason;
			}
		}
		throw new IllegalArgumentException("unknown code: " + code);
	}
}
