/************************************************************************
 * Cs2dLuaBinding by EnderCrypt                                         *
 * Copyright (C) 2022                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.library.cs2dlua.hook.data;

public class UpgradeProgress
{
	private final int total;
	private final int progress;
	
	public UpgradeProgress(int total, int progress)
	{
		this.total = total;
		this.progress = progress;
	}
	
	public int getTotal()
	{
		return total;
	}
	
	public int getProgress()
	{
		return progress;
	}
	
	public double getScale()
	{
		return 1.0 / getTotal() * getProgress();
	}
	
	public double getPercentage()
	{
		return getScale() * 100;
	}
}
