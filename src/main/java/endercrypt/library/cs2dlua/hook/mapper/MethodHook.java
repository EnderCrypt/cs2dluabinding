/************************************************************************
 * Cs2dLuaBinding by EnderCrypt                                         *
 * Copyright (C) 2022                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.library.cs2dlua.hook.mapper;


import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;

import org.luaj.vm2.LuaValue;
import org.luaj.vm2.Varargs;

import endercrypt.library.cs2dlua.hook.mapper.converter.LuaTypeConverter;
import endercrypt.library.cs2dlua.hook.result.collector.HookResult;
import endercrypt.library.cs2dlua.utility.Cs2dLuaBindUtility;


public class MethodHook
{
	private final Object instance;
	private final Method method;
	private final String hook;
	private final List<Class<?>> parameters;
	
	public MethodHook(Object instance, Method method, String hook)
	{
		this.instance = instance;
		this.method = method;
		this.hook = hook;
		this.parameters = Arrays.asList(method.getParameterTypes());
	}
	
	public Object getInstance()
	{
		return instance;
	}
	
	public Method getMethod()
	{
		return method;
	}
	
	public String getHook()
	{
		return hook;
	}
	
	public HookResult call(Varargs args) throws ReflectiveOperationException
	{
		List<LuaValue> argumentList = Cs2dLuaBindUtility.collectVarArgs(args);
		List<Object> argumentObjects = LuaTypeConverter.map(argumentList, parameters);
		return (HookResult) method.invoke(getInstance(), argumentObjects.toArray());
	}
}
