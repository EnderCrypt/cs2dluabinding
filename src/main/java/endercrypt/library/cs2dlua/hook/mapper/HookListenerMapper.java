/************************************************************************
 * Cs2dLuaBinding by EnderCrypt                                         *
 * Copyright (C) 2022                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.library.cs2dlua.hook.mapper;


import java.lang.reflect.InvocationTargetException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.Varargs;

import endercrypt.library.cs2dlua.hook.HookListener;
import endercrypt.library.cs2dlua.hook.result.collector.HookResult;
import endercrypt.library.cs2dlua.hook.result.impl.basic.LogResult;
import endercrypt.library.cs2dlua.hook.result.impl.basic.ParseResult;
import endercrypt.library.cs2dlua.hook.result.impl.basic.RconResult;
import endercrypt.library.cs2dlua.hook.result.impl.basic.TriggerEntityResult;
import endercrypt.library.cs2dlua.hook.result.impl.basic.TriggerResult;
import endercrypt.library.cs2dlua.hook.result.impl.object.HitZoneResult;
import endercrypt.library.cs2dlua.hook.result.impl.object.ObjectDamageResult;
import endercrypt.library.cs2dlua.hook.result.impl.object.ObjectUpgradeResult;
import endercrypt.library.cs2dlua.hook.result.impl.object.TurretScanResult;
import endercrypt.library.cs2dlua.hook.result.impl.player.AssistResult;
import endercrypt.library.cs2dlua.hook.result.impl.player.BombDefuseResult;
import endercrypt.library.cs2dlua.hook.result.impl.player.BombExplodeResult;
import endercrypt.library.cs2dlua.hook.result.impl.player.BombPlantResult;
import endercrypt.library.cs2dlua.hook.result.impl.player.BuildAttemptResult;
import endercrypt.library.cs2dlua.hook.result.impl.player.BuildResult;
import endercrypt.library.cs2dlua.hook.result.impl.player.BuyResult;
import endercrypt.library.cs2dlua.hook.result.impl.player.ConnectAttemptResult;
import endercrypt.library.cs2dlua.hook.result.impl.player.DieResult;
import endercrypt.library.cs2dlua.hook.result.impl.player.DominateResult;
import endercrypt.library.cs2dlua.hook.result.impl.player.DropResult;
import endercrypt.library.cs2dlua.hook.result.impl.player.FlagCaptureResult;
import endercrypt.library.cs2dlua.hook.result.impl.player.FlagTakeResult;
import endercrypt.library.cs2dlua.hook.result.impl.player.HitResult;
import endercrypt.library.cs2dlua.hook.result.impl.player.HostageDamageResult;
import endercrypt.library.cs2dlua.hook.result.impl.player.HostageKillResult;
import endercrypt.library.cs2dlua.hook.result.impl.player.HostageUseResult;
import endercrypt.library.cs2dlua.hook.result.impl.player.ItemFadeoutResult;
import endercrypt.library.cs2dlua.hook.result.impl.player.NameResult;
import endercrypt.library.cs2dlua.hook.result.impl.player.RadioResult;
import endercrypt.library.cs2dlua.hook.result.impl.player.SayResult;
import endercrypt.library.cs2dlua.hook.result.impl.player.SayTeamResult;
import endercrypt.library.cs2dlua.hook.result.impl.player.SpawnResult;
import endercrypt.library.cs2dlua.hook.result.impl.player.SuicideResult;
import endercrypt.library.cs2dlua.hook.result.impl.player.TeamResult;
import endercrypt.library.cs2dlua.hook.result.impl.player.VoteResult;
import endercrypt.library.cs2dlua.hook.result.impl.player.WalkOverResult;


/**
 * https://www.cs2d.com/help.php?hookcat=all
 *
 * @author endercrypt
 */
public class HookListenerMapper implements HookListener
{
	private static final Logger logger = LogManager.getLogger(HookListenerMapper.class);
	
	private final HookDispatcher dispatcher = new HookDispatcher(this);
	
	@Override
	public final HookResult onHook(String hook, Varargs args)
	{
		try
		{
			return dispatcher.call(hook, args);
		}
		catch (InvocationTargetException e)
		{
			logger.error("hook dispatcher failed with exception", e.getCause());
			return null;
		}
		catch (ReflectiveOperationException e)
		{
			logger.error("hook dispatcher failed", e);
			return null;
		}
	}
	
	// CUSTOM
	
	@Hook("init")
	public void onInit()
	{
		
	}
	
	// BASIC
	
	@SuppressWarnings("unused")
	@Hook("break")
	public void onBreak(int x, int y, int player)
	{
		
	}
	
	@SuppressWarnings("unused")
	@Hook("endround")
	public void onEndRound(int mode, int delay)
	{
		
	}
	
	@SuppressWarnings("unused")
	@Hook("httpdata")
	public void onHttpData(int requestId, int state, LuaValue data)
	{
		
	}
	
	@SuppressWarnings("unused")
	@Hook("log")
	public LogResult onLog(String text)
	{
		return null;
	}
	
	@SuppressWarnings("unused")
	@Hook("mapchange")
	public void onMapChange(String mapName)
	{
		
	}
	
	@SuppressWarnings("unused")
	@Hook("parse")
	public ParseResult onParse(String command)
	{
		return null;
	}
	
	@SuppressWarnings("unused")
	@Hook("projectile")
	public void onProjectile(int id, int weapon, int x, int y, int projectileId)
	{
		
	}
	
	@SuppressWarnings("unused")
	@Hook("projectile_impact")
	public void onProjectileImpact(int id, int weapon, int x, int y, int mode, int projectileId)
	{
		
	}
	
	@SuppressWarnings("unused")
	@Hook("rcon")
	public RconResult onRcon(String command, int id, String ip, int port)
	{
		return null;
	}
	
	@SuppressWarnings("unused")
	@Hook("shutdown")
	public void onShutdown()
	{
		
	}
	
	@SuppressWarnings("unused")
	@Hook("startround")
	public void onStartRound(int mode)
	{
		
	}
	
	@SuppressWarnings("unused")
	@Hook("startround_prespawn")
	public void onStartRoundPreSpawn(int mode)
	{
		
	}
	
	@SuppressWarnings("unused")
	@Hook("trigger")
	public TriggerResult onTrigger(String trigger, int source)
	{
		return null;
	}
	
	@SuppressWarnings("unused")
	@Hook("triggerentity")
	public TriggerEntityResult onTriggerEntity(int x, int y)
	{
		return null;
	}
	
	// TIME
	
	@SuppressWarnings("unused")
	@Hook("always")
	public void onAlways()
	{
		
	}
	
	@SuppressWarnings("unused")
	@Hook("minute")
	public void onMinute()
	{
		
	}
	
	@SuppressWarnings("unused")
	@Hook("ms100")
	public void onMs100()
	{
		
	}
	
	@SuppressWarnings("unused")
	@Hook("second")
	public void onSecond()
	{
		
	}
	
	// PLAYER
	
	@SuppressWarnings("unused")
	@Hook("assist")
	public AssistResult onAssist(int assist, int victim, int killer)
	{
		return null;
	}
	
	@SuppressWarnings("unused")
	@Hook("attack")
	public void onAttack(int id)
	{
		
	}
	
	@SuppressWarnings("unused")
	@Hook("attack2")
	public void onAttack2(int id, int mode)
	{
		
	}
	
	@SuppressWarnings("unused")
	@Hook("bombdefuse")
	public BombDefuseResult onBombDefuse(int id)
	{
		return null;
	}
	
	@SuppressWarnings("unused")
	@Hook("bombexplode")
	public BombExplodeResult onBombExplode(int id, int x, int y)
	{
		return null;
	}
	
	@SuppressWarnings("unused")
	@Hook("bombplant")
	public BombPlantResult onBombPlant(int id, int x, int y)
	{
		return null;
	}
	
	@SuppressWarnings("unused")
	@Hook("build")
	public BuildResult onBuild(int id, int type, int x, int y, int mode, int objectId)
	{
		return null;
	}
	
	@SuppressWarnings("unused")
	@Hook("buildattempt")
	public BuildAttemptResult onBuildAttempt(int id, int type, int x, int y, int mode)
	{
		return null;
	}
	
	@SuppressWarnings("unused")
	@Hook("buy")
	public BuyResult onBuy(int id, int weapon)
	{
		return null;
	}
	
	@SuppressWarnings("unused")
	@Hook("clientdata")
	public void onClientData(int id, int mode, LuaValue data1, LuaValue data2)
	{
		
	}
	
	@SuppressWarnings("unused")
	@Hook("clientsetting")
	public void onClientSetting(int id, int setting, LuaValue value1, LuaValue value2)
	{
		
	}
	
	@SuppressWarnings("unused")
	@Hook("collect")
	public void onCollect(int id, int iid, int type, int ain, int a, int mode)
	{
		
	}
	
	@SuppressWarnings("unused")
	@Hook("connect")
	public void onConnect(int id)
	{
		
	}
	
	@SuppressWarnings("unused")
	@Hook("connect_attempt")
	public ConnectAttemptResult onConnectAttempt(String name, String ip, int port, int usgnId, String usgnName, String steamId, String steamName)
	{
		return null;
	}
	
	@SuppressWarnings("unused")
	@Hook("connect_initplayer")
	public void onConnectInitPlayer(int id)
	{
		
	}
	
	@SuppressWarnings("unused")
	@Hook("die")
	public DieResult onDie(int victim, int killer, int weapon, int x, int y, int killerObject)
	{
		return null;
	}
	
	@SuppressWarnings("unused")
	@Hook("disconnect")
	public void onDisconnect(int id, int reason)
	{
		
	}
	
	@SuppressWarnings("unused")
	@Hook("dominate")
	public DominateResult onDominate(int id, int team, int x, int y)
	{
		return null;
	}
	
	@SuppressWarnings("unused")
	@Hook("drop")
	public DropResult onDrop(int id, int iid, int type, int ammoin, int ammo, int mode, int x, int y)
	{
		return null;
	}
	
	@SuppressWarnings("unused")
	@Hook("flagcapture")
	public FlagCaptureResult onFlagCapture(int id, int team, int x, int y)
	{
		return null;
	}
	
	@SuppressWarnings("unused")
	@Hook("flagtake")
	public FlagTakeResult onFlagTake(int id, int team, int x, int y)
	{
		return null;
	}
	
	@SuppressWarnings("unused")
	@Hook("flashlight")
	public void onFlashLight(int id, int state)
	{
		
	}
	
	@SuppressWarnings("unused")
	@Hook("hit")
	public HitResult onHit(int id, int source, int weapon, int hpdmg, int apdmg, int rawdmg, int object)
	{
		return null;
	}
	
	@SuppressWarnings("unused")
	@Hook("hostagedamage")
	public HostageDamageResult onHostageDamage(int id, int hostageId, int damage)
	{
		return null;
	}
	
	@SuppressWarnings("unused")
	@Hook("hostagekill")
	public HostageKillResult onHostageKill(int id, int hostageId, int damage)
	{
		return null;
	}
	
	@SuppressWarnings("unused")
	@Hook("hostagerescue")
	public void onHostageRescue(int id, int x, int y)
	{
		
	}
	
	@SuppressWarnings("unused")
	@Hook("hostageuse")
	public HostageUseResult onHostageUse(int id, int hostageId, int mode)
	{
		return null;
	}
	
	@SuppressWarnings("unused")
	@Hook("itemfadeout")
	public ItemFadeoutResult onItemFadeout(int iid, int type, int x, int y)
	{
		return null;
	}
	
	@SuppressWarnings("unused")
	@Hook("join")
	public void onJoin(int id)
	{
		
	}
	
	@SuppressWarnings("unused")
	@Hook("key")
	public void onKey(int id, String key, int state)
	{
		
	}
	
	@SuppressWarnings("unused")
	@Hook("kill")
	public void onKill(int killer, int victim, int weapon, int x, int y, int killerObject, int assistant)
	{
		
	}
	
	@SuppressWarnings("unused")
	@Hook("leave")
	public void onLeave(int id, int reason)
	{
		
	}
	
	@SuppressWarnings("unused")
	@Hook("menu")
	public void onMenu(int id, String title, int button)
	{
		
	}
	
	@SuppressWarnings("unused")
	@Hook("move")
	public void onMove(int id, int x, int y, int walk)
	{
		
	}
	
	@SuppressWarnings("unused")
	@Hook("movetile")
	public void onMoveTile(int id, int x, int y)
	{
		
	}
	
	@SuppressWarnings("unused")
	@Hook("name")
	public NameResult onName(int id, String oldName, String newName, int forced)
	{
		return null;
	}
	
	@SuppressWarnings("unused")
	@Hook("radio")
	public RadioResult onRadio(int id, String message)
	{
		return null;
	}
	
	@SuppressWarnings("unused")
	@Hook("reload")
	public void onReload(int id, int mode)
	{
		
	}
	
	@SuppressWarnings("unused")
	@Hook("say")
	public SayResult onSay(int id, String message)
	{
		return null;
	}
	
	@SuppressWarnings("unused")
	@Hook("sayteam")
	public SayTeamResult onSayTeam(int id, String message)
	{
		return null;
	}
	
	@SuppressWarnings("unused")
	@Hook("select")
	public void onSelect(int id, int type, int mode)
	{
		
	}
	
	@SuppressWarnings("unused")
	@Hook("serveraction")
	public void onServerAction(int id, int action)
	{
		
	}
	
	@SuppressWarnings("unused")
	@Hook("shieldhit")
	public void onSheildHit(int id, int source, int weapon, int attackDirection, int objectId, int damage)
	{
		
	}
	
	@SuppressWarnings("unused")
	@Hook("spawn")
	public SpawnResult onSpawn(int id)
	{
		return null;
	}
	
	@SuppressWarnings("unused")
	@Hook("specswitch")
	public void onSpecSwitch(int id, int target)
	{
		
	}
	
	@SuppressWarnings("unused")
	@Hook("spray")
	public void onSpray(int id)
	{
		
	}
	
	@SuppressWarnings("unused")
	@Hook("suicide")
	public SuicideResult onSuicide(int id)
	{
		return null;
	}
	
	@SuppressWarnings("unused")
	@Hook("team")
	public TeamResult onTeam(int id, int team, int look)
	{
		return null;
	}
	
	@SuppressWarnings("unused")
	@Hook("use")
	public void onUse(int id, int event, int data, int x, int y)
	{
		
	}
	
	@SuppressWarnings("unused")
	@Hook("usebutton")
	public void onUseButton(int id, int x, int y)
	{
		
	}
	
	@SuppressWarnings("unused")
	@Hook("vipescape")
	public void onVipEscape(int id, int x, int y)
	{
		
	}
	
	@SuppressWarnings("unused")
	@Hook("voice")
	public void onVoice(int id)
	{
		
	}
	
	@SuppressWarnings("unused")
	@Hook("vote")
	public VoteResult onVote(int id, int mode, LuaValue param)
	{
		return null;
	}
	
	@SuppressWarnings("unused")
	@Hook("walkover")
	public WalkOverResult onValkOver(int id, int iid, int type, int ain, int a, int mode)
	{
		return null;
	}
	
	// OBJECT
	
	@SuppressWarnings("unused")
	@Hook("hitzone")
	public HitZoneResult onHitZone(int imageId, int playerId, int objectId, int weapon, int x, int y, int damage)
	{
		return null;
	}
	
	@SuppressWarnings("unused")
	@Hook("objectdamage")
	public ObjectDamageResult onObjectDamage(int id, int damage, int player)
	{
		return null;
	}
	
	@SuppressWarnings("unused")
	@Hook("objectkill")
	public void onObjectKill(int id, int player)
	{
		
	}
	
	@SuppressWarnings("unused")
	@Hook("objectupgrade")
	public ObjectUpgradeResult onObjectUpgrade(int id, int player, int progress, int total)
	{
		return null;
	}
	
	@SuppressWarnings("unused")
	@Hook("turretscan")
	public TurretScanResult onTurretScan(int turretId, int team, int x, int y)
	{
		return null;
	}
}
