/************************************************************************
 * Cs2dLuaBinding by EnderCrypt                                         *
 * Copyright (C) 2022                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.library.cs2dlua.hook.mapper.converter;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.luaj.vm2.LuaValue;

import endercrypt.library.cs2dlua.hook.mapper.converter.impl.IntConverter;
import endercrypt.library.cs2dlua.hook.mapper.converter.impl.NoneConverter;
import endercrypt.library.cs2dlua.hook.mapper.converter.impl.StringConverter;


public class LuaTypeConverter
{
	private LuaTypeConverter()
	{
		throw new UnsupportedOperationException();
	}
	
	private static Map<Class<?>, LuaConverter<?>> converters = new HashMap<>();
	
	static
	{
		register(new IntConverter());
		register(new StringConverter());
		register(new NoneConverter());
	}
	
	public static void register(LuaConverter<?> converter)
	{
		converters.put(converter.getTargetClass(), converter);
	}
	
	@SuppressWarnings("unchecked")
	public static <T> T map(LuaValue value, Class<T> targetClass)
	{
		LuaConverter<?> converter = converters.get(targetClass);
		if (converter == null)
		{
			throw new IllegalArgumentException("unknown type: " + targetClass);
		}
		return (T) converter.convert(value);
	}
	
	public static List<Object> map(List<LuaValue> values, List<Class<?>> classes)
	{
		if (values.size() != classes.size())
		{
			throw new IllegalArgumentException("values and classes both have different sizes (" + values + " vs " + classes + ")");
		}
		List<Object> result = new ArrayList<>();
		for (int i = 0; i < Math.min(values.size(), classes.size()); i++)
		{
			LuaValue value = values.get(i);
			Class<?> targetClass = classes.get(i);
			result.add(map(value, targetClass));
		}
		return result;
	}
}
