/************************************************************************
 * Cs2dLuaBinding by EnderCrypt                                         *
 * Copyright (C) 2022                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.library.cs2dlua.hook.adapter;


import java.net.InetSocketAddress;

import org.luaj.vm2.LuaValue;
import org.luaj.vm2.Varargs;

import endercrypt.library.cs2dlua.cs2d.Cs2d;
import endercrypt.library.cs2dlua.cs2d.entities.image.Cs2dImage;
import endercrypt.library.cs2dlua.cs2d.entities.items.Cs2dItem;
import endercrypt.library.cs2dlua.cs2d.entities.itemtypes.Cs2dItemType;
import endercrypt.library.cs2dlua.cs2d.entities.objects.Cs2dObject;
import endercrypt.library.cs2dlua.cs2d.entities.objecttypes.Cs2dObjectType;
import endercrypt.library.cs2dlua.cs2d.entities.players.Cs2dPlayer;
import endercrypt.library.cs2dlua.cs2d.entities.players.properties.identity.Steam;
import endercrypt.library.cs2dlua.cs2d.entities.players.properties.identity.Usgn;
import endercrypt.library.cs2dlua.cs2d.entities.projectiles.Cs2dProjectile;
import endercrypt.library.cs2dlua.hook.HookListener;
import endercrypt.library.cs2dlua.hook.data.LeaveReason;
import endercrypt.library.cs2dlua.hook.data.ProjectileMode;
import endercrypt.library.cs2dlua.hook.data.RoundMode;
import endercrypt.library.cs2dlua.hook.data.Team;
import endercrypt.library.cs2dlua.hook.data.TriggerSource;
import endercrypt.library.cs2dlua.hook.data.UpgradeProgress;
import endercrypt.library.cs2dlua.hook.mapper.HookListenerMapper;
import endercrypt.library.cs2dlua.hook.result.collector.HookResult;
import endercrypt.library.cs2dlua.hook.result.impl.basic.LogResult;
import endercrypt.library.cs2dlua.hook.result.impl.basic.ParseResult;
import endercrypt.library.cs2dlua.hook.result.impl.basic.RconResult;
import endercrypt.library.cs2dlua.hook.result.impl.basic.TriggerEntityResult;
import endercrypt.library.cs2dlua.hook.result.impl.basic.TriggerResult;
import endercrypt.library.cs2dlua.hook.result.impl.object.HitZoneResult;
import endercrypt.library.cs2dlua.hook.result.impl.object.ObjectDamageResult;
import endercrypt.library.cs2dlua.hook.result.impl.object.ObjectUpgradeResult;
import endercrypt.library.cs2dlua.hook.result.impl.object.TurretScanResult;
import endercrypt.library.cs2dlua.hook.result.impl.player.AssistResult;
import endercrypt.library.cs2dlua.hook.result.impl.player.BombDefuseResult;
import endercrypt.library.cs2dlua.hook.result.impl.player.BombExplodeResult;
import endercrypt.library.cs2dlua.hook.result.impl.player.BombPlantResult;
import endercrypt.library.cs2dlua.hook.result.impl.player.BuildAttemptResult;
import endercrypt.library.cs2dlua.hook.result.impl.player.BuildResult;
import endercrypt.library.cs2dlua.hook.result.impl.player.BuyResult;
import endercrypt.library.cs2dlua.hook.result.impl.player.ConnectAttemptResult;
import endercrypt.library.cs2dlua.hook.result.impl.player.DieResult;
import endercrypt.library.cs2dlua.hook.result.impl.player.DominateResult;
import endercrypt.library.cs2dlua.hook.result.impl.player.SayResult;
import endercrypt.library.cs2dlua.hook.result.impl.player.SpawnResult;
import endercrypt.library.cs2dlua.utility.position.PixelPosition;
import endercrypt.library.cs2dlua.utility.position.Position;
import endercrypt.library.cs2dlua.utility.position.TilePosition;


public class HookListenerAdapter implements HookListener
{
	private final Cs2d cs2d;
	private final Adapter adapter = new Adapter();
	
	public HookListenerAdapter(Cs2d cs2d)
	{
		this.cs2d = cs2d;
	}
	
	public Cs2d getCs2d()
	{
		return cs2d;
	}
	
	@Override
	public final HookResult onHook(String hook, Varargs args)
	{
		return adapter.onHook(hook, args);
	}
	
	// CUSTOM
	
	@SuppressWarnings("unused")
	public void onInit()
	{
		
	}
	
	// BASIC
	
	@SuppressWarnings("unused")
	public void onBreak(Cs2dPlayer player, TilePosition position)
	{
		
	}
	
	@SuppressWarnings("unused")
	public void onEndRound(int delay, RoundMode mode)
	{
		
	}
	
	// TODO: consider removing entirely
	@SuppressWarnings("unused")
	public void onHttpData(int requestId, boolean success, LuaValue data)
	{
		
	}
	
	@SuppressWarnings("unused")
	public LogResult onLog(String text)
	{
		return null;
	}
	
	@SuppressWarnings("unused")
	public void onMapChange(String mapName)
	{
		
	}
	
	@SuppressWarnings("unused")
	public ParseResult onParse(String command)
	{
		return null;
	}
	
	@SuppressWarnings("unused")
	public void onProjectile(Cs2dPlayer player, Cs2dItemType weapon, PixelPosition position, Cs2dProjectile projectile)
	{
		
	}
	
	@SuppressWarnings("unused")
	public void onProjectileImpact(Cs2dPlayer player, Cs2dItemType weapon, PixelPosition position, ProjectileMode mode, Cs2dProjectile projectile)
	{
		
	}
	
	@SuppressWarnings("unused")
	public RconResult onRcon(Cs2dPlayer player, String command, InetSocketAddress address)
	{
		return null;
	}
	
	@SuppressWarnings("unused")
	public void onShutdown()
	{
		
	}
	
	@SuppressWarnings("unused")
	public void onStartRound(RoundMode mode)
	{
		
	}
	
	@SuppressWarnings("unused")
	public void onStartRoundPreSpawn(RoundMode mode)
	{
		
	}
	
	@SuppressWarnings("unused")
	public TriggerResult onTrigger(String trigger, TriggerSource source)
	{
		return null;
	}
	
	@SuppressWarnings("unused")
	public TriggerEntityResult onTriggerEntity(PixelPosition position)
	{
		return null;
	}
	
	// TIME
	
	@SuppressWarnings("unused")
	public void onAlways()
	{
		
	}
	
	@SuppressWarnings("unused")
	public void onMinute()
	{
		
	}
	
	@SuppressWarnings("unused")
	public void onMs100()
	{
		
	}
	
	@SuppressWarnings("unused")
	public void onSecond()
	{
		
	}
	
	// PLAYER
	
	@SuppressWarnings("unused")
	public AssistResult onAssist(Cs2dPlayer assist, Cs2dPlayer victim, Cs2dPlayer killer)
	{
		return null;
	}
	
	@SuppressWarnings("unused")
	public void onAttack(Cs2dPlayer player)
	{
		
	}
	
	@SuppressWarnings("unused")
	public void onAttack2(Cs2dPlayer player, int mode)
	{
		
	}
	
	@SuppressWarnings("unused")
	public BombDefuseResult onBombDefuse(Cs2dPlayer player)
	{
		return null;
	}
	
	@SuppressWarnings("unused")
	public BombPlantResult onBombPlant(Cs2dPlayer player, TilePosition position)
	{
		return null;
	}
	
	@SuppressWarnings("unused")
	public BombExplodeResult onBombExplode(Cs2dPlayer player, TilePosition position)
	{
		return null;
	}
	
	@SuppressWarnings("unused")
	public BuildResult onBuild(Cs2dPlayer player, TilePosition position, Cs2dObject object, int mode)
	{
		return null;
	}
	
	@SuppressWarnings("unused")
	public BuildAttemptResult onBuildAttempt(Cs2dPlayer player, Cs2dObjectType objectType, TilePosition position, int mode)
	{
		return null;
	}
	
	@SuppressWarnings("unused")
	public BuyResult onBuy(Cs2dPlayer player, Cs2dItemType itemType)
	{
		return null;
	}
	
	@SuppressWarnings("unused")
	public Object onClientData(Cs2dPlayer player, int mode, LuaValue data1, LuaValue data2)
	{
		return null;
	}
	
	@SuppressWarnings("unused")
	public void onCollect(Cs2dPlayer player, Cs2dItem item)
	{
		
	}
	
	@SuppressWarnings("unused")
	public void onConnect(Cs2dPlayer player)
	{
		
	}
	
	@SuppressWarnings("unused")
	public DominateResult onDominate(Cs2dPlayer player, Team team, PixelPosition position)
	{
		return null;
	}
	
	@SuppressWarnings("unused")
	public void onDisconnect(Cs2dPlayer player, LeaveReason leaveReason)
	{
		
	}
	
	@SuppressWarnings("unused")
	public DieResult onDie(Cs2dPlayer victim, Cs2dPlayer killer, Cs2dItemType weapon, PixelPosition position, Cs2dObject killerObject)
	{
		return null;
	}
	
	@SuppressWarnings("unused")
	public void onConnectInitPlayer(Cs2dPlayer player)
	{
		
	}
	
	@SuppressWarnings("unused")
	public ConnectAttemptResult onConnectAttempt(String name, InetSocketAddress address, Usgn usgn, Steam steam)
	{
		return null;
	}
	
	// UNSORTED
	
	@SuppressWarnings("unused")
	public SayResult onSay(Cs2dPlayer player, String message)
	{
		return null;
	}
	
	@SuppressWarnings("unused")
	public void onJoin(Cs2dPlayer player)
	{
		
	}
	
	@SuppressWarnings("unused")
	public void onLeave(Cs2dPlayer player, LeaveReason reason)
	{
		
	}
	
	@SuppressWarnings("unused")
	public SpawnResult onSpawn(Cs2dPlayer player)
	{
		return null;
	}
	
	// OBJECT
	
	@SuppressWarnings("unused")
	public HitZoneResult onHitZone(Cs2dImage image, Cs2dPlayer player, Cs2dObject object, Cs2dItemType itemType, PixelPosition position, int damage)
	{
		return null;
	}
	
	@SuppressWarnings("unused")
	public ObjectDamageResult onObjectDamage(Cs2dObject object, int damage, Cs2dPlayer source)
	{
		return null;
	}
	
	@SuppressWarnings("unused")
	public void onObjectKill(Cs2dObject object, Cs2dPlayer source)
	{
		
	}
	
	@SuppressWarnings("unused")
	public ObjectUpgradeResult onObjectUpgrade(Cs2dObject object, Cs2dPlayer source, UpgradeProgress progress)
	{
		return null;
	}
	
	@SuppressWarnings("unused")
	public TurretScanResult onTurretScan(Cs2dObject turret, Team team, PixelPosition position)
	{
		return null;
	}
	
	private class Adapter extends HookListenerMapper
	{
		private Cs2dPlayer mapPlayer(int id)
		{
			return getCs2d().getPlayers().get(id).get();
		}
		
		private Cs2dItemType mapItemType(int id)
		{
			return getCs2d().getItemTypes().get(id).get();
		}
		
		private Cs2dItem mapItem(int id)
		{
			return getCs2d().getItems().get(id).get();
		}
		
		private Cs2dObjectType mapObjectType(int id)
		{
			return getCs2d().getObjectTypes().get(id).get();
		}
		
		private Cs2dObject mapObject(int id)
		{
			return getCs2d().getObjects().get(id).orElse(null);
		}
		
		private Cs2dImage mapImage(int id)
		{
			return getCs2d().getImages().get(id).orElse(null);
		}
		
		// CUSTOM
		
		@Override
		public void onInit()
		{
			HookListenerAdapter.this.onInit();
		}
		
		// BASIC
		
		@Override
		public void onBreak(int x, int y, int player)
		{
			HookListenerAdapter.this.onBreak(mapPlayer(player), Position.tile(x, player));
		}
		
		@Override
		public void onEndRound(int mode, int delay)
		{
			HookListenerAdapter.this.onEndRound(delay, RoundMode.find(mode));
		}
		
		@Override
		public void onHttpData(int requestId, int state, LuaValue data)
		{
			HookListenerAdapter.this.onHttpData(requestId, state == 1, data);
		}
		
		@Override
		public LogResult onLog(String text)
		{
			return HookListenerAdapter.this.onLog(text);
		}
		
		@Override
		public void onMapChange(String mapName)
		{
			HookListenerAdapter.this.onMapChange(mapName);
		}
		
		@Override
		public ParseResult onParse(String command)
		{
			return HookListenerAdapter.this.onParse(command);
		}
		
		@Override
		public void onProjectile(int id, int weapon, int x, int y, int projectileId)
		{
			Cs2dPlayer player = mapPlayer(id);
			Cs2dItemType itemType = mapItemType(weapon);
			PixelPosition position = Position.pixel(x, y);
			Cs2dProjectile projectile = player.getProjectiles().get(projectileId).get();
			HookListenerAdapter.this.onProjectile(player, itemType, position, projectile);
		}
		
		@Override
		public void onProjectileImpact(int id, int weapon, int x, int y, int mode, int projectileId)
		{
			Cs2dPlayer player = mapPlayer(id);
			Cs2dItemType itemType = mapItemType(weapon);
			PixelPosition position = Position.pixel(x, y);
			ProjectileMode projectileMode = ProjectileMode.find(mode);
			Cs2dProjectile projectile = player.getProjectiles().get(projectileId).get();
			HookListenerAdapter.this.onProjectileImpact(player, itemType, position, projectileMode, projectile);
		}
		
		@Override
		public RconResult onRcon(String command, int id, String ip, int port)
		{
			return HookListenerAdapter.this.onRcon(mapPlayer(id), command, InetSocketAddress.createUnresolved(ip, port));
		}
		
		@Override
		public void onShutdown()
		{
			HookListenerAdapter.this.onShutdown();
		}
		
		@Override
		public void onStartRound(int mode)
		{
			HookListenerAdapter.this.onStartRound(RoundMode.find(mode));
		}
		
		@Override
		public void onStartRoundPreSpawn(int mode)
		{
			HookListenerAdapter.this.onStartRoundPreSpawn(RoundMode.find(mode));
		}
		
		@Override
		public TriggerResult onTrigger(String trigger, int source)
		{
			return HookListenerAdapter.this.onTrigger(trigger, TriggerSource.find(source));
		}
		
		@Override
		public TriggerEntityResult onTriggerEntity(int x, int y)
		{
			return HookListenerAdapter.this.onTriggerEntity(Position.pixel(x, y));
		}
		
		// TIME
		
		@Override
		public void onAlways()
		{
			HookListenerAdapter.this.onAlways();
		}
		
		@Override
		public void onMinute()
		{
			HookListenerAdapter.this.onMinute();
		}
		
		@Override
		public void onMs100()
		{
			HookListenerAdapter.this.onMs100();
		}
		
		@Override
		public void onSecond()
		{
			HookListenerAdapter.this.onSecond();
		}
		
		// PLAYER
		
		@Override
		public AssistResult onAssist(int assist, int victim, int killer)
		{
			return HookListenerAdapter.this.onAssist(mapPlayer(assist), mapPlayer(victim), mapPlayer(killer));
		}
		
		@Override
		public void onAttack(int id)
		{
			HookListenerAdapter.this.onAttack(mapPlayer(id));
		}
		
		@Override
		public void onAttack2(int id, int mode)
		{
			HookListenerAdapter.this.onAttack2(mapPlayer(id), mode);
		}
		
		@Override
		public BombDefuseResult onBombDefuse(int id)
		{
			return HookListenerAdapter.this.onBombDefuse(mapPlayer(id));
		}
		
		@Override
		public BombExplodeResult onBombExplode(int id, int x, int y)
		{
			return HookListenerAdapter.this.onBombExplode(mapPlayer(id), Position.tile(x, y));
		}
		
		@Override
		public BombPlantResult onBombPlant(int id, int x, int y)
		{
			return HookListenerAdapter.this.onBombPlant(mapPlayer(id), Position.tile(x, y));
		}
		
		@Override
		public BuildResult onBuild(int id, int type, int x, int y, int mode, int objectId)
		{
			return HookListenerAdapter.this.onBuild(mapPlayer(id), Position.tile(x, y), mapObject(objectId), mode);
		}
		
		@Override
		public BuildAttemptResult onBuildAttempt(int id, int type, int x, int y, int mode)
		{
			return HookListenerAdapter.this.onBuildAttempt(mapPlayer(id), mapObjectType(type), Position.tile(x, y), mode);
		}
		
		@Override
		public BuyResult onBuy(int id, int weapon)
		{
			return HookListenerAdapter.this.onBuy(mapPlayer(id), mapItemType(weapon));
		}
		
		@Override
		public void onClientData(int id, int mode, LuaValue data1, LuaValue data2)
		{
			HookListenerAdapter.this.onClientData(mapPlayer(id), mode, data1, data2);
		}
		
		@Override
		public void onCollect(int id, int iid, int type, int ain, int a, int mode)
		{
			HookListenerAdapter.this.onCollect(mapPlayer(id), mapItem(iid));
		}
		
		@Override
		public void onConnect(int id)
		{
			HookListenerAdapter.this.onConnect(mapPlayer(id));
		}
		
		@Override
		public ConnectAttemptResult onConnectAttempt(String name, String ip, int port, int usgnId, String usgnName, String steamId, String steamName)
		{
			InetSocketAddress address = InetSocketAddress.createUnresolved(ip, port);
			Usgn usgn = new Usgn(usgnId, steamName);
			Steam steam = new Steam(steamId, steamName);
			return HookListenerAdapter.this.onConnectAttempt(name, address, usgn, steam);
		}
		
		@Override
		public void onConnectInitPlayer(int id)
		{
			HookListenerAdapter.this.onConnectInitPlayer(mapPlayer(id));
		}
		
		@Override
		public DieResult onDie(int victim, int killer, int weapon, int x, int y, int killerObject)
		{
			Cs2dPlayer argVictim = mapPlayer(victim);
			Cs2dPlayer argKiller = mapPlayer(killer);
			Cs2dItemType argWeapon = mapItemType(weapon);
			PixelPosition argPosition = Position.pixel(x, y);
			Cs2dObject argKillerObject = mapObject(killerObject);
			return HookListenerAdapter.this.onDie(argVictim, argKiller, argWeapon, argPosition, argKillerObject);
		}
		
		@Override
		public void onDisconnect(int id, int reason)
		{
			HookListenerAdapter.this.onDisconnect(mapPlayer(id), LeaveReason.find(reason));
		}
		
		@Override
		public DominateResult onDominate(int id, int team, int x, int y)
		{
			return HookListenerAdapter.this.onDominate(mapPlayer(id), Team.find(team), Position.pixel(x, y));
		}
		
		// UNSORTED
		
		@Override
		public SayResult onSay(int id, String message)
		{
			return HookListenerAdapter.this.onSay(mapPlayer(id), message);
		}
		
		@Override
		public void onJoin(int id)
		{
			HookListenerAdapter.this.onJoin(mapPlayer(id));
		}
		
		@Override
		public void onLeave(int id, int reason)
		{
			HookListenerAdapter.this.onLeave(mapPlayer(id), LeaveReason.find(reason));
		}
		
		@Override
		public SpawnResult onSpawn(int id)
		{
			return HookListenerAdapter.this.onSpawn(mapPlayer(id));
		}
		
		// OBJECT
		
		@Override
		public HitZoneResult onHitZone(int imageId, int playerId, int objectId, int weapon, int x, int y, int damage)
		{
			Cs2dImage image = mapImage(imageId);
			Cs2dPlayer player = mapPlayer(playerId);
			Cs2dObject object = mapObject(objectId);
			Cs2dItemType itemType = mapItemType(weapon);
			PixelPosition position = Position.pixel(x, y);
			
			return HookListenerAdapter.this.onHitZone(image, player, object, itemType, position, damage);
		}
		
		@Override
		public ObjectDamageResult onObjectDamage(int id, int damage, int player)
		{
			Cs2dObject object = mapObject(id);
			Cs2dPlayer argPlayer = player == 0 ? null : mapPlayer(player);
			return HookListenerAdapter.this.onObjectDamage(object, damage, argPlayer);
		}
		
		@Override
		public void onObjectKill(int id, int player)
		{
			Cs2dObject object = mapObject(id);
			Cs2dPlayer argPlayer = player == 0 ? null : mapPlayer(player);
			HookListenerAdapter.this.onObjectKill(object, argPlayer);
		}
		
		@Override
		public ObjectUpgradeResult onObjectUpgrade(int id, int player, int progress, int total)
		{
			Cs2dObject object = mapObject(id);
			Cs2dPlayer argPlayer = mapPlayer(player);
			UpgradeProgress argProgress = new UpgradeProgress(total, progress);
			return HookListenerAdapter.this.onObjectUpgrade(object, argPlayer, argProgress);
		}
		
		@Override
		public TurretScanResult onTurretScan(int turretId, int team, int x, int y)
		{
			Cs2dObject turret = mapObject(turretId);
			Team argTeam = Team.find(team);
			PixelPosition position = Position.pixel(x, y);
			return HookListenerAdapter.this.onTurretScan(turret, argTeam, position);
		}
	}
}
