/************************************************************************
 * Cs2dLuaBinding by EnderCrypt                                         *
 * Copyright (C) 2022                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.library.cs2dlua.hook.listener;


import java.util.HashSet;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.luaj.vm2.Varargs;

import endercrypt.library.cs2dlua.hook.HookListener;
import endercrypt.library.cs2dlua.hook.result.collector.HookResult;
import endercrypt.library.cs2dlua.hook.result.collector.HookResultCollector;


@SuppressWarnings("serial")
public class ListenerGroup extends HashSet<HookListener>
{
	private static final Logger logger = LogManager.getLogger(ListenerGroup.class);
	
	public HookResultCollector execute(String hook, Varargs args)
	{
		HookResultCollector result = new HookResultCollector();
		for (HookListener listener : this)
		{
			result.merge(execute(listener, hook, args));
		}
		return result;
	}
	
	private HookResult execute(HookListener listener, String hook, Varargs args)
	{
		try
		{
			return listener.onHook(hook, args);
		}
		catch (Exception e)
		{
			logger.error("error on listener " + listener, e);
			return null;
		}
	}
}
