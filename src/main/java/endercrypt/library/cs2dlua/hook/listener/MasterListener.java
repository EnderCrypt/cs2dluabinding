/************************************************************************
 * Cs2dLuaBinding by EnderCrypt                                         *
 * Copyright (C) 2022                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.library.cs2dlua.hook.listener;


import org.luaj.vm2.LuaValue;
import org.luaj.vm2.Varargs;

import endercrypt.library.cs2dlua.hook.HookListener;
import endercrypt.library.cs2dlua.hook.result.collector.HookResultCollector;


public class MasterListener
{
	private final ListenerGroup preListeners = new ListenerGroup();
	private final ListenerGroup listeners = new ListenerGroup();
	private final ListenerGroup postListeners = new ListenerGroup();
	
	public void addPreListener(HookListener listener)
	{
		preListeners.add(listener);
	}
	
	public void addListener(HookListener listener)
	{
		listeners.add(listener);
	}
	
	public void addPostListener(HookListener listener)
	{
		postListeners.add(listener);
	}
	
	public LuaValue execute(String hook, Varargs args)
	{
		HookResultCollector collector = new HookResultCollector();
		collector.merge(preListeners.execute(hook, args));
		collector.merge(listeners.execute(hook, args));
		collector.merge(postListeners.execute(hook, args));
		return collector.generate();
	}
}
