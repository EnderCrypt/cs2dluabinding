/************************************************************************
 * Cs2dLuaBinding by EnderCrypt                                         *
 * Copyright (C) 2022                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.library.cs2dlua.utility;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public abstract class BasicThread implements Runnable
{
	private static final Logger logger = LogManager.getLogger(BasicThread.class);
	
	@Override
	public final void run()
	{
		try
		{
			configureThread(Thread.currentThread());
			runThread();
		}
		catch (InterruptedException | UncheckedInterruptedException e)
		{
			return;
		}
		catch (Exception e)
		{
			onThreadException(e);
		}
	}
	
	@SuppressWarnings("unused")
	protected void configureThread(Thread thread)
	{
		
	}
	
	protected abstract void runThread() throws Exception;
	
	protected void onThreadException(Exception e)
	{
		logger.error("thread " + Thread.currentThread().getName() + " crashed", e);
	}
}
