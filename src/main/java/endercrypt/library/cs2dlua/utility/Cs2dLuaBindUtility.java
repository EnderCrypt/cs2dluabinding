/************************************************************************
 * Cs2dLuaBinding by EnderCrypt                                         *
 * Copyright (C) 2022                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.library.cs2dlua.utility;


import java.util.ArrayList;
import java.util.List;

import org.luaj.vm2.LuaTable;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.Varargs;


public class Cs2dLuaBindUtility
{
	private Cs2dLuaBindUtility()
	{
		throw new UnsupportedOperationException();
	}
	
	public static List<LuaValue> collectVarArgs(Varargs varArgs)
	{
		List<LuaValue> array = new ArrayList<>();
		for (int i = 1; i <= varArgs.narg(); i++)
		{
			array.add(varArgs.arg(i));
		}
		return array;
	}
	
	public static List<LuaValue> collectIpairs(LuaTable table)
	{
		List<LuaValue> array = new ArrayList<>();
		int index = 1;
		LuaValue value = null;
		while ((value = table.get(index)) != LuaValue.NIL)
		{
			array.add(value);
			index++;
		}
		return array;
	}
}
