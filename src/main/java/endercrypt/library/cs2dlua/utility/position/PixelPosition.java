/************************************************************************
 * Cs2dLuaBinding by EnderCrypt                                         *
 * Copyright (C) 2022                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.library.cs2dlua.utility.position;

public class PixelPosition extends Position
{
	protected PixelPosition(int x, int y)
	{
		super(x, y);
	}
	
	@Override
	public TilePosition negate()
	{
		return new TilePosition(-x, -y);
	}
	
	// CONVERT
	
	@Override
	public TilePosition asTilePosition()
	{
		double half = (TilePosition.SIZE / 2);
		int cx = (int) Math.floor((x - half) / TilePosition.SIZE);
		int cy = (int) Math.floor((y - half) / TilePosition.SIZE);
		return new TilePosition(cx, cy);
	}
	
	@Override
	public PixelPosition asPixelPosition()
	{
		return this;
	}
	
	public PixelPosition alignWithTiles()
	{
		return asTilePosition().asPixelPosition();
	}
	
	// ADD
	
	@Override
	public TilePosition add(TilePosition position)
	{
		return asTilePosition().add(position);
	}
	
	@Override
	public PixelPosition add(PixelPosition position)
	{
		return new PixelPosition(x + position.x, y + position.y);
	}
	
	// SUBTRACT
	
	@Override
	public Position subtract(TilePosition position)
	{
		return asTilePosition().add(position);
	}
	
	@Override
	public Position subtract(PixelPosition position)
	{
		return new PixelPosition(x - position.x, y - position.y);
	}
	
	// TOSTRING
	
	@Override
	public String toString()
	{
		return "PixelPosition [x=" + x + ", y=" + y + "]";
	}
}
