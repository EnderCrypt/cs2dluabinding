/************************************************************************
 * Cs2dLuaBinding by EnderCrypt                                         *
 * Copyright (C) 2022                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.library.cs2dlua.utility.position;

public class TilePosition extends Position
{
	public static final int SIZE = 32;
	
	protected TilePosition(int x, int y)
	{
		super(x, y);
	}
	
	@Override
	public TilePosition negate()
	{
		return new TilePosition(-x, -y);
	}
	
	// CONVERT
	
	@Override
	public TilePosition asTilePosition()
	{
		return this;
	}
	
	@Override
	public PixelPosition asPixelPosition()
	{
		double half = (TilePosition.SIZE / 2);
		int cx = (int) Math.floor((x * TilePosition.SIZE) + half);
		int cy = (int) Math.floor((y * TilePosition.SIZE) + half);
		return new PixelPosition(cx, cy);
	}
	
	// ADD
	
	@Override
	public TilePosition add(TilePosition position)
	{
		return new TilePosition(x + position.x, y + position.y);
	}
	
	@Override
	public PixelPosition add(PixelPosition position)
	{
		return asPixelPosition().add(position);
	}
	
	// SUBTRACT
	
	@Override
	public Position subtract(TilePosition position)
	{
		return new TilePosition(x - position.x, y - position.y);
	}
	
	@Override
	public Position subtract(PixelPosition position)
	{
		return asPixelPosition().subtract(position);
	}
	
	// TOSTRING
	
	@Override
	public String toString()
	{
		return "TilePosition [x=" + x + ", y=" + y + "]";
	}
}
