/************************************************************************
 * Cs2dLuaBinding by EnderCrypt                                         *
 * Copyright (C) 2022                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.library.cs2dlua.utility.position;

public abstract class Position
{
	// FACTORY
	
	public static PixelPosition pixel(int x, int y)
	{
		return new PixelPosition(x, y);
	}
	
	public static TilePosition tile(int x, int y)
	{
		return new TilePosition(x, y);
	}
	
	public final int x;
	public final int y;
	
	protected Position(int x, int y)
	{
		this.x = x;
		this.y = y;
	}
	
	public abstract Position negate();
	
	// CONVERT
	
	public abstract TilePosition asTilePosition();
	
	public abstract PixelPosition asPixelPosition();
	
	// ADD
	
	public abstract Position add(TilePosition position);
	
	public abstract Position add(PixelPosition position);
	
	// SUBTRACT
	
	public abstract Position subtract(TilePosition position);
	
	public abstract Position subtract(PixelPosition position);
	
	// DISTANCE
	
	public double distanceTo(Position position)
	{
		return asPixelPosition().subtract(position.asPixelPosition()).distance();
	}
	
	public double distance()
	{
		return Math.sqrt((x * x) + (y * y));
	}
	
	// DIRECTION
	
	public double directionTo(Position position)
	{
		return asPixelPosition().subtract(position.asPixelPosition()).direction();
	}
	
	public double direction()
	{
		return Math.atan2(y, x);
	}
	
	// TOSTRING
	
	@Override
	public String toString()
	{
		return "Position [x=" + x + ", y=" + y + "]";
	}
}
