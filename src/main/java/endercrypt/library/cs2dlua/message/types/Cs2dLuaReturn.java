/************************************************************************
 * Cs2dLuaBinding by EnderCrypt                                         *
 * Copyright (C) 2022                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.library.cs2dlua.message.types;


import org.luaj.vm2.LuaTable;
import org.luaj.vm2.LuaValue;

import endercrypt.library.cs2dlua.message.EventType;
import endercrypt.library.cs2dlua.message.factory.Cs2dMessage;
import endercrypt.library.cs2dlua.message.factory.Cs2dMessageProducer;


public class Cs2dLuaReturn extends Cs2dMessage
{
	private final LuaValue value;
	private final String error;
	
	public Cs2dLuaReturn(LuaValue value)
	{
		this.value = value;
		this.error = null;
	}
	
	public Cs2dLuaReturn(String error)
	{
		this.value = LuaValue.NIL;
		this.error = error;
	}
	
	public LuaValue getValue()
	{
		return value;
	}
	
	public String getError()
	{
		return error;
	}
	
	@Override
	public EventType getType()
	{
		return EventType.luaReturn;
	}
	
	@Override
	public LuaTable assemblePackage()
	{
		LuaTable table = super.assemblePackage();
		table.set("value", getValue());
		return table;
	}
	
	public static class Producer implements Cs2dMessageProducer
	{
		@Override
		public EventType getEventType()
		{
			return EventType.luaReturn;
		}
		
		@Override
		public Cs2dMessage produce(LuaTable lua)
		{
			LuaValue error = lua.get("error");
			if (error != LuaValue.NIL)
			{
				return new Cs2dLuaReturn(error.checkjstring());
			}
			LuaValue value = lua.get("value");
			return new Cs2dLuaReturn(value);
		}
	}
}
