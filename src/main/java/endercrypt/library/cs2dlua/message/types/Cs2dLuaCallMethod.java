/************************************************************************
 * Cs2dLuaBinding by EnderCrypt                                         *
 * Copyright (C) 2022                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.library.cs2dlua.message.types;


import org.luaj.vm2.LuaTable;
import org.luaj.vm2.LuaValue;

import endercrypt.library.cs2dlua.message.EventType;
import endercrypt.library.cs2dlua.message.factory.Cs2dMessage;
import endercrypt.library.cs2dlua.message.factory.Cs2dMessageProducer;


public class Cs2dLuaCallMethod extends Cs2dMessage
{
	public static Cs2dLuaCallMethod assemble(String method, LuaValue... args)
	{
		return new Cs2dLuaCallMethod(method, LuaValue.listOf(args));
	}
	
	private final String method;
	private final LuaTable args;
	
	public Cs2dLuaCallMethod(String method, LuaTable args)
	{
		this.method = method;
		this.args = args;
	}
	
	public String getMethod()
	{
		return method;
	}
	
	public LuaTable getArgs()
	{
		return args;
	}
	
	@Override
	public EventType getType()
	{
		return EventType.luaCallMethod;
	}
	
	@Override
	public LuaTable assemblePackage()
	{
		LuaTable table = super.assemblePackage();
		table.set("method", getMethod());
		table.set("args", getArgs());
		return table;
	}
	
	public static class Producer implements Cs2dMessageProducer
	{
		@Override
		public EventType getEventType()
		{
			return EventType.luaCallMethod;
		}
		
		@Override
		public Cs2dMessage produce(LuaTable lua)
		{
			String method = lua.get("method").checkjstring();
			LuaTable args = lua.get("args").checktable();
			return new Cs2dLuaCallMethod(method, args);
		}
	}
}
