/************************************************************************
 * Cs2dLuaBinding by EnderCrypt                                         *
 * Copyright (C) 2022                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.library.cs2dlua.message.types;


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.luaj.vm2.LuaTable;
import org.luaj.vm2.LuaValue;

import endercrypt.library.cs2dlua.message.EventType;
import endercrypt.library.cs2dlua.message.factory.Cs2dMessage;
import endercrypt.library.cs2dlua.message.factory.Cs2dMessageProducer;
import endercrypt.library.cs2dlua.utility.Cs2dLuaBindUtility;


public class Cs2dHookStart extends Cs2dMessage
{
	private final String hook;
	private final List<LuaValue> args;
	
	public Cs2dHookStart(String hook, List<LuaValue> args)
	{
		this.hook = hook;
		this.args = Collections.unmodifiableList(new ArrayList<>(args));
	}
	
	public String getHook()
	{
		return hook;
	}
	
	public List<LuaValue> getArgs()
	{
		return args;
	}
	
	@Override
	public EventType getType()
	{
		return EventType.hookStart;
	}
	
	public static class Producer implements Cs2dMessageProducer
	{
		@Override
		public EventType getEventType()
		{
			return EventType.hookStart;
		}
		
		@Override
		public Cs2dMessage produce(LuaTable lua)
		{
			String hook = lua.get("hook").checkjstring();
			List<LuaValue> args = Cs2dLuaBindUtility.collectIpairs(lua.get("args").checktable());
			return new Cs2dHookStart(hook, args);
		}
	}
}
