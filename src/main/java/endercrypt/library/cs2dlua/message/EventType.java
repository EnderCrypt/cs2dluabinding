/************************************************************************
 * Cs2dLuaBinding by EnderCrypt                                         *
 * Copyright (C) 2022                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.library.cs2dlua.message;

public enum EventType
{
	hookStart("HOOK_START"), // we receive
	luaCall("LUA_CALL"), // we send (and get response)
	luaCallMethod("LUA_CALL_METHOD"), // we send (and get response)
	luaReturn("LUA_RETURN"), // we receive
	hookEnd("HOOK_END"), // we send (and forget)
	ping("PING"); // ping
	
	private final String key;
	
	private EventType(String key)
	{
		this.key = key;
	}
	
	public String getKey()
	{
		return key;
	}
	
	public static EventType find(String key)
	{
		for (EventType messageType : values())
		{
			if (messageType.getKey().equals(key))
			{
				return messageType;
			}
		}
		throw new IllegalArgumentException("unknown message key: " + key);
	}
}
