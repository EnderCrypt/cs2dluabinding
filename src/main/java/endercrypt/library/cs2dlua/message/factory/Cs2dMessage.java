/************************************************************************
 * Cs2dLuaBinding by EnderCrypt                                         *
 * Copyright (C) 2022                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.library.cs2dlua.message.factory;


import java.util.Objects;

import org.apache.commons.lang3.StringUtils;
import org.luaj.vm2.LuaTable;

import endercrypt.library.cs2dlua.lua.LuaManager;
import endercrypt.library.cs2dlua.message.EventType;


public abstract class Cs2dMessage
{
	private static final int PACKAGE_SIZE_LENGTH = 4;
	
	public abstract EventType getType();
	
	public LuaTable assemblePackage()
	{
		return new LuaTable();
	}
	
	private LuaTable constructPayload()
	{
		LuaTable payload = assemblePackage();
		Objects.requireNonNull(payload, "payload");
		payload.set("type", getType().getKey());
		return payload;
	}
	
	public String assemblePackage(LuaManager luaManager)
	{
		LuaTable payload = constructPayload();
		
		String payloadString = luaManager.serialize(payload);
		
		String payloadLengthString = StringUtils.leftPad(String.valueOf(payloadString.length()), PACKAGE_SIZE_LENGTH, " ");
		
		return payloadLengthString + payloadString;
	}
}
