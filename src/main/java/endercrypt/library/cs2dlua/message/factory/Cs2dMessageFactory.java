/************************************************************************
 * Cs2dLuaBinding by EnderCrypt                                         *
 * Copyright (C) 2022                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.library.cs2dlua.message.factory;


import java.util.EnumMap;

import org.luaj.vm2.LuaTable;

import endercrypt.library.cs2dlua.message.EventType;
import endercrypt.library.cs2dlua.message.types.Cs2dHookEnd;
import endercrypt.library.cs2dlua.message.types.Cs2dHookStart;
import endercrypt.library.cs2dlua.message.types.Cs2dLuaCall;
import endercrypt.library.cs2dlua.message.types.Cs2dLuaReturn;
import endercrypt.library.cs2dlua.message.types.Cs2dPing;


public class Cs2dMessageFactory
{
	private final EnumMap<EventType, Cs2dMessageProducer> producers = new EnumMap<>(EventType.class);
	
	public Cs2dMessageFactory()
	{
		register(new Cs2dLuaCall.Producer());
		register(new Cs2dLuaReturn.Producer());
		register(new Cs2dPing.Producer());
		register(new Cs2dHookStart.Producer());
		register(new Cs2dHookEnd.Producer());
	}
	
	private void register(Cs2dMessageProducer producer)
	{
		EventType eventType = producer.getEventType();
		producers.put(eventType, producer);
	}
	
	public Cs2dMessage build(LuaTable payload)
	{
		EventType eventType = EventType.find(payload.get("type").checkjstring());
		Cs2dMessageProducer producer = producers.get(eventType);
		Cs2dMessage message = producer.produce(payload);
		if (message.getType() != eventType)
		{
			throw new IllegalArgumentException("producer produced illegal object");
		}
		return message;
	}
}
