/************************************************************************
 * Cs2dLuaBinding by EnderCrypt                                         *
 * Copyright (C) 2022                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.library.cs2dlua;


import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import endercrypt.library.cs2dlua.cs2d.Cs2d;
import endercrypt.library.cs2dlua.cs2d.entities.players.Cs2dPlayer;
import endercrypt.library.cs2dlua.hook.adapter.HookListenerAdapter;
import endercrypt.library.cs2dlua.hook.result.impl.player.SayResult;
import endercrypt.library.cs2dlua.hook.result.impl.player.SpawnResult;
import endercrypt.library.cs2dlua.utility.position.Position;
import endercrypt.library.intellicommand.IntelliCommand;
import endercrypt.library.intellicommand.bundle.Bundle;
import endercrypt.library.intellicommand.exception.execution.CommandArgumentMismatchException;
import endercrypt.library.intellicommand.exception.execution.CommandNotFoundException;
import endercrypt.library.intellicommand.exception.execution.MalformedCommandException;
import endercrypt.library.intellicommand.exception.execution.UnderlyingCommandException;


public class Cs2dTestListener extends HookListenerAdapter
{
	private static final Logger logger = LogManager.getLogger(Cs2dTestListener.class);
	
	private static final String commandPrefix = "!";
	private static final IntelliCommand intelliCommand = new IntelliCommand();
	
	public Cs2dTestListener(Cs2d cs2d)
	{
		super(cs2d);
		intelliCommand.commands().register(new TestCommands());
	}
	
	@Override
	public SpawnResult onSpawn(Cs2dPlayer player)
	{
		getCs2d().message("player " + player.getName() + " at " + player.getPixelPosition());
		
		player.setPosition(Position.tile(10, 10));
		
		return SpawnResult.withWeapons(5, 32, 57);
	}
	
	@Override
	public SayResult onSay(Cs2dPlayer player, String message)
	{
		if (message.toLowerCase().startsWith(commandPrefix.toLowerCase()))
		{
			String commandString = message.substring(commandPrefix.length());
			Bundle bundle = new Bundle();
			bundle.attach().instance("cs2d", getCs2d());
			bundle.attach().instance("player", player);
			try
			{
				intelliCommand.execute(commandString, bundle);
			}
			catch (MalformedCommandException e)
			{
				player.message(e.getMessage());
			}
			catch (CommandArgumentMismatchException e)
			{
				player.message("did you mean?\n\t - " + e.getCommands().stream().map(c -> c.getPrimarySignature().toString()).collect(Collectors.joining("\n\t - ")));
			}
			catch (CommandNotFoundException e)
			{
				player.message("Command not found");
			}
			catch (UnderlyingCommandException e)
			{
				player.message("internal error");
				logger.error("failed to run command: " + commandString, e.getCause());
			}
			
			return SayResult.dontShowMessage();
		}
		return null;
	}
}
