/************************************************************************
 * Cs2dLuaBinding by EnderCrypt                                         *
 * Copyright (C) 2022                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.library.cs2dlua;


import java.awt.Dimension;

import endercrypt.library.cs2dlua.cs2d.Cs2d;
import endercrypt.library.cs2dlua.cs2d.entities.image.Cs2dImage;
import endercrypt.library.cs2dlua.cs2d.entities.image.HitZoneListener;
import endercrypt.library.cs2dlua.cs2d.entities.image.ImageHitZoneEffect;
import endercrypt.library.cs2dlua.cs2d.entities.image.configuration.ImageConfiguration;
import endercrypt.library.cs2dlua.cs2d.entities.image.configuration.ImageConfigurationFollowPlayer.ImagePlayerMode;
import endercrypt.library.cs2dlua.cs2d.entities.image.configuration.ImageConfigurationFollowPlayer.ImagePlayerRotate;
import endercrypt.library.cs2dlua.cs2d.entities.image.configuration.ImageConfigurationFreeFloat.ImageLayerMode;
import endercrypt.library.cs2dlua.cs2d.entities.image.source.ImageSource;
import endercrypt.library.cs2dlua.cs2d.entities.items.Cs2dItem;
import endercrypt.library.cs2dlua.cs2d.entities.itemtypes.Cs2dItemType;
import endercrypt.library.cs2dlua.cs2d.entities.objects.Cs2dObject;
import endercrypt.library.cs2dlua.cs2d.entities.players.Cs2dPlayer;
import endercrypt.library.cs2dlua.hook.result.impl.object.HitZoneResult;
import endercrypt.library.cs2dlua.utility.position.PixelPosition;
import endercrypt.library.cs2dlua.utility.position.Position;
import endercrypt.library.intellicommand.annotation.CommandSignature;
import endercrypt.library.intellicommand.annotation.Include;


@CommandSignature("")
public class TestCommands
{
	@CommandSignature("print items")
	public void printItems(@Include("cs2d") Cs2d cs2d)
	{
		for (Cs2dItem item : cs2d.getItems().getAll())
		{
			Cs2dItemType itemType = item.getType();
			String price = itemType.getPrice().map(String::valueOf).orElse("None");
			cs2d.message(item.getPosition() + " -> " + itemType.getName() + " (worth: " + price + ")");
		}
	}
	
	@CommandSignature("print itemtypes")
	public void printItemTypes(@Include("cs2d") Cs2d cs2d)
	{
		for (Cs2dItemType itemType : cs2d.getItemTypes().getAll())
		{
			String price = itemType.getPrice().map(String::valueOf).orElse("None");
			cs2d.message(itemType.getName() + " (worth: " + price + ")");
		}
	}
	
	@CommandSignature("print players")
	public void printPlayers(@Include("cs2d") Cs2d cs2d)
	{
		for (Cs2dPlayer player : cs2d.getPlayers().getAll())
		{
			cs2d.message(player.getName() + " (position: " + player.getTilePosition() + ")");
		}
	}
	
	@CommandSignature("imageme")
	public void imageMe(@Include("cs2d") Cs2d cs2d, @Include("player") Cs2dPlayer player)
	{
		ImageSource source = ImageSource.fromFlag("SE");
		ImageConfiguration configuration = ImageConfiguration.followingPlayer(player, ImagePlayerMode.abovePlayer, ImagePlayerRotate.rotateWithPlayer, true);
		cs2d.getImages().create(source, configuration);
	}
	
	@CommandSignature("float")
	public void test(@Include("cs2d") Cs2d cs2d, @Include("player") Cs2dPlayer player)
	{
		ImageSource source = ImageSource.fromFile("gfx/botAlert.png");
		ImageConfiguration configuration = ImageConfiguration.freeFloating(ImageLayerMode.top, Position.tile(3, 3));
		Cs2dImage image = cs2d.getImages().create(source, configuration);
		image.configureHitZone(ImageHitZoneEffect.redBlood, true, new Dimension(-16, -16), new Dimension(32, 32));
		image.registerHitZoneListener(new HitZoneListener()
		{
			@Override
			public HitZoneResult onHitzone(Cs2dPlayer player, Cs2dObject object, Cs2dItemType itemType, PixelPosition position, int damage)
			{
				image.free();
				return null;
			}
		});
	}
}
